import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

/**
 *Program drukujący wszystkie słowa ze strumienia przy użyciu zbioru
 */
public class SetTest
{
    public static void main(String[]args)
    {
        Set<String> words = new HashSet<>();
        long totalTime = 0;


        try (Scanner in = new Scanner(System.in))
        {
            do {
                String word = in.next();
                long callTime = System.currentTimeMillis();
                words.add(word);
                callTime = System.currentTimeMillis() - callTime;
                totalTime += callTime;


            }while (in.hasNext());



        }

        Iterator<String> iter = words.iterator();
        for (int i=1; i<=20 && iter.hasNext(); i++)
            System.out.println(iter.next());
        System.out.println(". . .");
        System.out.println(words.size() +  " niepowtarzających się słów. " + totalTime + "milisekund.");
    }
}
