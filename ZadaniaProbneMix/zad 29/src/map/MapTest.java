package map;

import java.util.*;

/**
 * Program demonstrujący użycie słownika z kluczami typu String i wartościami typu Employee
 */

// OBCZAIĆ O CO TU CHODZI!!!!!!!!!!!!!!!!!!!!!
public class MapTest
{
    public static void main(String[] args)
    {
        Map<String, Employee> staff = new HashMap<>();
        staff.put("144-25-5464", new Employee("Anna Kowalska"));
        staff.put("567-24-2546", new Employee("Henryk Kwiatek"));
        staff.put("157-62-7935", new Employee("Marcin Nowak"));
        staff.put("456-62-5527", new Employee("Franciszek Frankowski"));

        //Wydruk wszystkich pozycji
        System.out.println(staff);

        //Usunięcie wartości
        staff.remove("567-24-2546");

        //Podmienienie pozycji
        staff.put("456-62-552", new Employee("Weronika Kowalska"));

        //Wyszukiwanie wartości
        System.out.println(staff.get("157-62-7935"));

        //iteracja przez wszystkiie pozycje
        staff.forEach((k, v) -> System.out.println("klucz=" + k + "Wartość=" + v ));

    }
}
