import java.time.LocalDate;

public class EmployeeTest
{
    public static void main(String[]args)
    {
        Employee[] staff = new Employee[3];// Tworzymy Tablice o nazwie Employee i wielkosci 3

        staff[0] = new Employee("Robert Sanczez",75000,1987,12,15);//Wstawiamy obiekty reprezentujace
        staff[1] = new Employee("Jacek Cydejko",50000,1989,10,1);// pracownikow
        staff[2] = new Employee("Mariusz Romańczuk",40000,1990,3,15);

        for (Employee e : staff)//Zwiekszenie pnsji wszystkich pracowników o podany%
            e.raiseSalary(10);

        for(Employee e : staff) {// drukuje wszystkie dane zapisane w tabeli zaktualizowane przez powyższą pętlę.
            System.out.println("Name = " + e.getName() + " ,salary = " + e.getSalary() + " , hireDay = " + e.getHireDay());
        }
    }
}
class Employee// klasa Employee do ktorej odnosi sie klasa EmployeeTest
{

    private String name;//Dane powinny być private zeby zachowac hermetyzacje
    private Double salary;//Tzn zeby nie mogly byc odczytywane lub znieniane przez
    private LocalDate hireDay;//jakies obiekty czy klasy z dupy

    public Employee(String n, double s, int year, int month, int day)//To jest konstruktor new Employee
            //Trzeba oczywiscie zdefiniowac nazwac itp te pierdoły w konstruktorze.
    {
        name = n;
        salary = s;
        hireDay = LocalDate.of(year, month, day);
    }

    public String getName()//pobiera imie
    {
        return name;  //zwraca imie
    }

    public Double getSalary()//pobiera wysokość wynagrodzenia
    {
        return salary;// zwraca
    }

    public LocalDate getHireDay()// pobiera 3 wartosci(date urodzin)
    {
        return hireDay;//zwraca date uro
    }

    public void raiseSalary(double byPercent)//Zwiekszenie salary przez podanie wartosci byPercent w petli
    {
        double raise = salary * byPercent/100;// zmienna raise = obecna salary * podany % podwyżki
        salary += raise;// zwieksza salary o wartość podwyżki
    }
}