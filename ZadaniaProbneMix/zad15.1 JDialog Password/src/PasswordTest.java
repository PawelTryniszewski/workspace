import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PasswordTest extends JFrame implements ActionListener
{

    private PanelHasła panelHasła;

    private JTextArea notatnik;
    private JScrollPane scrollPane;
    private JButton bDodajUżytkownika;

   public PasswordTest()
   {
      setTitle("Okno Dialogowe");
      setSize(400,400);
      setLayout(null);

      notatnik = new JTextArea();
      notatnik.setWrapStyleWord(true);
      scrollPane = new JScrollPane(notatnik);
      scrollPane.setBounds(0,0,300,200);
      add(scrollPane);

      bDodajUżytkownika = new JButton("Dodaj Użytkownika");
      bDodajUżytkownika.setBounds(0,250,150,20);
      add(bDodajUżytkownika);
      bDodajUżytkownika.addActionListener(this);
  }
    public static void main(String[]args)
    {
      PasswordTest app = new PasswordTest();
      app.setDefaultCloseOperation(EXIT_ON_CLOSE);
      app.setVisible(true);

    }



    @Override
    public void actionPerformed(ActionEvent e)
    {
        if(panelHasła==null)
            panelHasła = new PanelHasła(null);
        panelHasła.setVisible(true);
        panelHasła.setFocus();

        if(panelHasła.isOK())
        {
            notatnik.append(panelHasła.getUser()+",");
            notatnik.append(panelHasła.getPassword() + "\n");


        }

  }
}
class PanelHasła extends JDialog implements ActionListener
{
    private JLabel lUser, lHasło;
    private JTextField tUser;
    private JPasswordField tHasło;
    private JButton bOK, bCancel;
    private boolean okData;

    public PanelHasła(JFrame owner)
    {

        super(owner,"Wprowadzenie Hasła",true);
        setSize(300,200);
        setLayout(null);

        lUser = new JLabel("Użytkownik",JLabel.RIGHT);
        lUser.setBounds(0,0,100,20);
        add(lUser);

        tUser = new JTextField();
        tUser.setBounds(150,0,100,20);
        add(tUser);

        lHasło = new JLabel("Hasło",JLabel.RIGHT);
        lHasło.setBounds(0,50,100,20);
        add(lHasło);

        tHasło = new JPasswordField();
        tHasło.setBounds(150,50,100,20);
        add(tHasło);

        bOK = new JButton("OK");
        bOK.setBounds(0,100,100,20);
        bOK.addActionListener(this);
        add(bOK);

        bCancel = new JButton("Cancle");
        bCancel.setBounds(150,100,100,20);
        bCancel.addActionListener(this);
        add(bCancel);

    }
    public String getUser()
    {
        return tUser.getText();
    }
    public String getPassword()
    {
        return new String(tHasło.getPassword());
    }
    public boolean isOK()
    {
        return okData;
    }
    public void setFocus()
    {
        tUser.requestFocusInWindow();
    }



    @Override
    public void actionPerformed(ActionEvent e)
    {
        Object z = e.getSource();

        if(z==bOK)
            okData = true;
        else
            okData = false;
        setVisible(false);
    }
}
