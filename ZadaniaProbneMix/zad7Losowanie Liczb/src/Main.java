import java.io.*;
import java.util.*;
public class Main
{
    public static void main(String[] args)
            throws IOException
    {
        double losuj_liczbe, zgadnij_liczbe;
        BufferedReader br = new BufferedReader(new
                InputStreamReader(System.in));// Wpisywanie liczby.
        System.out.println("Program losuje liczbę z przedziału od 0 do 9. ");
        System.out.println("Zgadnij tę liczbę.");
        Random r = new Random();
        losuj_liczbe = Math.round(10*( r.nextDouble()));
        zgadnij_liczbe = Double.parseDouble(br.readLine());
        if (zgadnij_liczbe == losuj_liczbe)
        {
            System.out.println("Gratulacje! Odgadłeś liczbę!");
        }
        else
        {
            System.out.print("Bardzo mi przykro, ale wylosowana liczba to ");
                    System.out.println((int) losuj_liczbe + ".");//Liczba, Która zostala wylosowana.
        }
    }
}