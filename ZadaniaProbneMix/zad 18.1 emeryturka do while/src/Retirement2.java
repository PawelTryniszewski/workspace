import java.util.Scanner;

public class Retirement2
{
    private static String input;

    public static void main(String[]args)
    {
        Scanner in = new Scanner(System.in);// Wczytywanie danych

       
        System.out.print("Ile rocznie bedziesz wpłacał?");
        double payment = in.nextDouble();

        System.out.print("Stopa procentowa w %:");
        double interestRate = in.nextDouble();

        double balance = 0;
        int years = 0;


        //Aktualizacja salda jeśli cel nie został osiągniety(Zwiększamy licznik i aktualizujemy bieżącą kwotę, aż suma przekroczy
        // wyznaczoną kwotę
        do 
        {
            balance += payment;// do balance dodajemy to ile będziemy rocznie wpłacać
            double interest = balance * interestRate/100;// interest to odsetki z tego co wplacilismy
            balance += interest;// kapusta+odsetki w danym roku( do nowej wartosci balance dodajemy wartość interest)
            years++;//zwiekszamy o kolejny rok jesli...
            
            //Drukowanie aktualnego stanu konta
            System.out.printf("Po upływie %d lat stan Twojego konta wyniesie %,.2f%n",years,balance);
            
            //Zapytanie o gotowość przejścia na emeryture
            System.out.print("Chcesz przejść na emeryture? (T/N)");
            input = in.next();
            
        }

        while(input.equals("N"));// Pętla bedzie powtarzana z years++ za każdym razem, kiedy wybierzemy odpowiedź N!!


    }
}


