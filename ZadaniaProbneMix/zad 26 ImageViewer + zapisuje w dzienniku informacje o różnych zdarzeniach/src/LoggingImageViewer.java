import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.*;

/**
 * Zmodyfikowana przeglądarka grafiki, ktora zapisuje w dzienniku informacje o różnych zdarzeniach.
 */
public class LoggingImageViewer
{

    public static void main(String[]args)
    {
        if (System.getProperty("java.util.logging.config.class") == null && System.getProperty("java.util.logging.config.file") == null)
        {
            try
            {
                Logger.getLogger("com.horstmann.corejava").setLevel(Level.ALL);//Zwraca/tworzy rejestrator
                final int LOG_ROTATION_CUNT = 10;
                Handler handler = new FileHandler("%h/LoggingImageViwer.log", 0, LOG_ROTATION_CUNT);
                Logger.getLogger("com.horstmann.corejava").addHandler(handler);
            }
            catch (IOException e)
            {
                Logger.getLogger("com.horstmann.corejava").log(Level.SEVERE,"Nie można utworzyć obiektu obsługi pliku dziennika.", e);
            }
        }

        EventQueue.invokeLater(() ->
        {
            Handler windowHandler = new WindowHandler();
            windowHandler.setLevel(Level.ALL);
                Logger.getLogger("com.horstmann.corejava").addHandler(windowHandler);

                JFrame frame = new ImageViewerFrame();
                frame.setTitle("LoggingImageViewer");
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

                Logger.getLogger("com.horstmann.corejava").fine("Wyświetlanie ramki");
                frame.setVisible(true);

        });
    }

}

/**
 * Ramka zawierająca obraz
 */

class ImageViewerFrame extends JFrame
{
    private static final int DEFAULT_WIDTH = 300;
    private static final int DEFAULT_HEIGHT = 400;

    private JLabel label;
    private static Logger logger = Logger.getLogger("com.horstmann.corejava");

    public ImageViewerFrame()
    {
        logger.entering("ImageViewerFrame", "<init>");
        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);

        //Pasek menu
        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);

        JMenu menu = new JMenu("Plik");
        menuBar.add(menu);

        JMenuItem openItem = new JMenuItem("Otwórz");
        menu.add(openItem);
        openItem.addActionListener(new FileOpenListener());

        JMenuItem exitItem = new JMenuItem("Zamknij");
        menu.add(exitItem);
        exitItem.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent e)
                {
                    logger.fine("Zamykanie.");
                    System.exit(0);
                }
            });

        //Etykieta

        label = new JLabel();
        add(label);
        logger.exiting("ImageViewerFrame", "<init>");
    }

    private class FileOpenListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            logger.entering("ImageViewerFrame.FileOpenListener","actionPerformed", e);

            //Okno wyboru plików

            JFileChooser chooser = new JFileChooser();
            chooser.setCurrentDirectory(new File("."));

            //Akceptowanie wszystkich pilków z rozszerzeniem gif.

            chooser.setFileFilter(new javax.swing.filechooser.FileFilter()
            {
                @Override
                public boolean accept(File f)
                {
                    return f.getName().toLowerCase().endsWith(">gif")||f.isDirectory();
                }
                public String getDescription()
                {
                    return "Obrazy GIF";
                }
            });

            //Wyświetlanie okna dialogowego wyboru plików.
            int r = chooser.showOpenDialog(ImageViewerFrame.this);

            //Jeśli plik obrazu został zaakceptowany, jest on ustawiany jako ikona etykiety
            if (r== JFileChooser.APPROVE_OPTION)
            {
                String name = chooser.getSelectedFile().getPath();
                logger.log(Level.FINE, "Wczytywanie pliku {0}", name);
                label.setIcon(new ImageIcon(name));
            }
            else logger.fine("Anulowano okno otwierania pliku.");
            logger.exiting("ImageViewerFrame.FileOpenListener", "actionPerformed");
        }
    }
}

/**
 * Klasa obsługi wyświetlania rekordów dziennika w oknie.
 *
 */

class WindowHandler extends StreamHandler
{
    private JFrame frame;
    public WindowHandler()
    {
        frame = new JFrame();
        final JTextArea output = new JTextArea();
        output.setEditable(false);
        frame.setSize(200,200);
        frame.add(new JScrollPane(output));
        frame.setFocusableWindowState(false);
        frame.setVisible(true);
        setOutputStream(new OutputStream() {
            @Override
            public void write(int b)
            {

            }
            public void write(byte[] b, int off, int len)
            {
                output.append((new String(b, off, len)));
            }
        });
    }
    public void publish(LogRecord record)
    {
        if (!frame.isVisible()) return;
        super.publish(record);
        flush();
    }
}