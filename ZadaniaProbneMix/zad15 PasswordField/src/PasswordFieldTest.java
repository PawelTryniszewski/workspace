import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PasswordFieldTest extends JFrame implements ActionListener
{
    JPasswordField pHasło;
    public PasswordFieldTest()
    {
        setTitle("Test Hasła");
        setSize(300,300);
        setLayout(null);


        pHasło = new JPasswordField();
        pHasło.setBounds(50,50,150,20);
        pHasło.addActionListener(this);
        add(pHasło);
    }

    public static void main(String[] args)
    {
        PasswordFieldTest app = new PasswordFieldTest();
        app.setDefaultCloseOperation(EXIT_ON_CLOSE);
        app.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        JOptionPane.showMessageDialog(null, String.valueOf(pHasło.getPassword()));
    }
}
