import java.util.Scanner;

public class Main{
    public static void main(String args[])
    {
       int a;//Deklerujemy, ze istnieje zmienna a
       Scanner scanner = new Scanner(System.in);//Dodaje do programu skaner
       System.out.print("a=");//drukuje treść
       a = scanner.nextInt();// Podajemy wartość dla zmiennej a

       if(a<11)// instrukcja warunkowa ( jesli a jest mniejsze od 11)
       {
           System.out.println("Mniejsze od 11");// system drukuje tresc z cudzyslowow
       }
       else if(a==11)// kolejna instrukcja warunkowa
       {
           System.out.println("Równe 11");
       }

       else// wykonuje kiedy żadna z powyższych instrukcji nie jest spelniona
           System.out.println("Wieksze od 11");

    }
}