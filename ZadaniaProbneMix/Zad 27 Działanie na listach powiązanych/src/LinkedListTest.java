import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 * Program demonstrujący działanie na listach powiązanych.
 */

public class LinkedListTest
{
    public static void main(String[]args)
    {
        List<String> a = new LinkedList<>();
        a.add("Ania");
        a.add("Karol");
        a.add("Eryk");

        List<String> b = new LinkedList<>();
        b.add("Bartek");
        b.add("Daniel");
        b.add("Franek");
        b.add("Gosia");

        //Scalanie list a i b

        ListIterator<String> alert = a.listIterator();
        Iterator<String> bIter = b.iterator();

        while (bIter.hasNext())
        {
            if(alert.hasNext()) alert.next();
            alert.add(bIter.next());
        }

        System.out.println(a);

        //Usunięcie co drugiego słowa z listy b

        bIter = b.iterator();
        while(bIter.hasNext())
        {
            bIter.next();//Opuszczenie jednego elementu
            if (bIter.hasNext())
            {
                bIter.next(); //Opuszczenie następnego elementu
                bIter.remove(); //Usunięcie elementu
            }
        }

        System.out.println(b);

        //Usuniecie wszystkich słów znajdujących sie w liście b z listy a

        a.removeAll(b);
        System.out.println(a);
        /**
         * TO WYJEBALO SŁOWA ZNAJDUJĄCE SIE NA LIŚCIE B(Z KTÓREJ WCZEŚNIEJ ZOSTALO WYJEBANE CO DRUGIE SŁOWO!!!!)
         * ZE SCALONEJ LISTY A+B
         */
    }
}

