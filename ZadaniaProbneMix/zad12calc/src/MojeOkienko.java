import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

public class MojeOkienko extends JFrame implements ActionListener //JFrame okienko
{
    JButton bPodajDate;
    JButton bWyjście;
    JLabel lWyświetlDate;
    public MojeOkienko()
    {
        setSize(350,200);
        setTitle("Moje pierwsze okno");
        setLayout(null);
        bPodajDate = new JButton("Podaj datę");
        bPodajDate.setBounds(50,50,100,20);
        Component add = add(bPodajDate);// add wyswietla date bo na dole jest okreslone zrodlo
        bPodajDate.addActionListener(this);// add wyswietla date bo na dole jest okreslone zrodlo
        bWyjście = new JButton("Wyjście");
        bWyjście.setBounds(150,50,100,20);
        add (bWyjście);// add zamyka okno bo na dole jest okreslone zrodlo
        bWyjście.addActionListener(this);// add zamyka okno bo na dole jest okreslone zrodlo
        lWyświetlDate = new JLabel("Data:");//wyswietla w ramce date
        lWyświetlDate.setBounds(50,100,650,20);
        lWyświetlDate.setForeground(Color.LIGHT_GRAY);
        lWyświetlDate.setFont(new Font("SansSerif", Font.BOLD, 16));
        add(lWyświetlDate);

    }
    public static void main(String[]args)
    {
        MojeOkienko okienko = new MojeOkienko();
        okienko.setDefaultCloseOperation(EXIT_ON_CLOSE);// zamyka sie po nacisnieciu x
        okienko.setVisible(true);// okno pojawia sie (bolean true)
    }

    @Override
    public void actionPerformed(ActionEvent e)//implementuje nadanie akcji przyciskom
    {
        Object źródło = e.getSource();//ustalenie zrodla sygnalu
        if(źródło==bPodajDate)
        {
            lWyświetlDate.setText(new Date().toString());
            //System.out.println(new Date());
        }
        else if(źródło==bWyjście)
        {
            dispose();//zamyka okno
        }
    }

}
