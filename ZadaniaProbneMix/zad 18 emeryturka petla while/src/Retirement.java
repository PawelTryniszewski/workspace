import java.util.Scanner;

public class Retirement
{
    public static void main(String[]args)
    {
        Scanner in = new Scanner(System.in);// Wczytywanie danych

        System.out.print("Jle pieniedzy potrzebujesz, żeby przejsc na emeryture?");
        double goal = in.nextDouble();

        System.out.print("Ile rocznie bedziesz wpłacał?");
        double payment = in.nextDouble();

        System.out.print("Stopa procentowa w %:");
        double interestRate = in.nextDouble();

        double balance = 0;
        int years = 0;


        //Aktualizacja salda jeści cel nie został osiągniety

        while (balance<goal)
        {
            balance += payment;
            double interest = balance * interestRate/100;
            balance += interest;
            years++;
        }

        System.out.println("Możesz przejść na emeryture za "+years+" lat ");


    }
}
