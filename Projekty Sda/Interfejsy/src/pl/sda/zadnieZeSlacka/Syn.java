package pl.sda.zadnieZeSlacka;

public class Syn implements CzlonekRodziny {
    private String imie;

    public String getImie() {
        return imie;
    }

    public Syn(String imie) {
        this.imie = imie;

    }

    @Override
    public void przedstawSie() {
        System.out.println("who is asking? "+getImie());

    }

    @Override
    public boolean jestDorosły() {
        return false;
    }
}
