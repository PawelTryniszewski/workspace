package pl.sda.zadnieZeSlacka;

public class Corka implements CzlonekRodziny {
    private String imie;

    public Corka(String imie) {
        this.imie = imie;
    }

    public String getImie() {
        return imie;
    }

    @Override
    public void przedstawSie() {
        System.out.println("I'm doughter "+getImie());

    }

    @Override
    public boolean jestDorosły() {
        return false;
    }
}
