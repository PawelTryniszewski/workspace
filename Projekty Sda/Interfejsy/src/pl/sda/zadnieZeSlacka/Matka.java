package pl.sda.zadnieZeSlacka;

public class Matka implements CzlonekRodziny {
    private String imie;

    public Matka(String imie) {
        this.imie = imie;
    }

    public String getImie() {
        return imie;
    }

    @Override
    public void przedstawSie() {
        System.out.println("I'm mother. "+getImie());

    }

    @Override
    public boolean jestDorosły() {
        return true;
    }
}
