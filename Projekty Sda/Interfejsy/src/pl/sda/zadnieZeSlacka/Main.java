package pl.sda.zadnieZeSlacka;

import java.lang.reflect.Array;
import java.util.ArrayList;

import static javafx.scene.input.KeyCode.T;

/**
 * Utwórz interfejs ICzłonekRodziny i metodą przedstawSie():void. Wykorzystaj interfejs i implementuj go w klasach Matka,
 * Ojciec, Syn, Córka.
 * dla klasy Matka niech instancja wypisuje wartość “i am mother”,
 * dla klasy Ojciec niech instancja wypisuje wartość “i am your father”,
 * dla klasy Córka niech instancja wypisuje wartość “i am daughter ;) ”,
 * dla klasy Syn niech instancja wypisuje wartość “who’s asking?”
 * Dopisz w interfejsie dodatkową metodę jestDorosły():boolean która zwraca dla rodziców true, dla dzieci false.
 * Stwórz w mainie kilka instancji powyższych klas. Przechowaj je w liście, a następnie iteruj i wypisuj w kolejnych
 * liniach ich metody ‘przedstawSie()’ oraz “jestDorosły”
 * we wszystkich klasach dodaj pole imie.
 * Zmień metodę ‘przedstawSie()’ tak, aby poza treścią wypisywała również imie członka rodziny. Imie przypisuj w konstruktorze
 * *Zamień metodę przedstawSie():void na defaultową. Domyślnie metoda ma wypisać “I am just a simple family member”.
 */
public class Main {

    public static void main(String[] args) {
        Corka Ania = new Corka("Ania");
        Matka Basia = new Matka("Basia");
        Ojciec Adam = new Ojciec("Adam");
        Syn Karol = new Syn("Karol");
        CzlonekRodziny[] rodzina = new CzlonekRodziny[]{Ania,Basia,Adam,Karol};
        ArrayList<CzlonekRodziny> rodzina1 = new ArrayList();
        rodzina1.add(Ania);
        rodzina1.add(Basia);
        rodzina1.add(Adam);
        rodzina1.add(Karol);

        for (CzlonekRodziny czlonek:rodzina1) {
            System.out.println(czlonek.jestDorosły());
            czlonek.przedstawSie();
            if (czlonek instanceof Ojciec){
                Ojciec ojciec= (Ojciec) czlonek;
                ojciec.zawolajOPiwo();
            }

        }

        System.out.println();

        for (CzlonekRodziny czlonekRodziny:rodzina) {
            czlonekRodziny.przedstawSie();
            System.out.println(czlonekRodziny.jestDorosły());

            if (czlonekRodziny instanceof Ojciec){
                Ojciec ojciec= (Ojciec) czlonekRodziny;
                ojciec.zawolajOPiwo();
            }

        }

        Ojciec znaleziony = znajdzOjca(rodzina  );
        if(znaleziony != null) {
            znaleziony.przedstawSie();
        }


    }
        private static <T extends CzlonekRodziny> Ojciec znajdzOjca(T[] rodzina){
            for (int i = 0; i < rodzina.length; i++) {
                if (rodzina[i] instanceof Ojciec) {
                    return (Ojciec) rodzina[i];
                }
            }

            return null;
        }
}
