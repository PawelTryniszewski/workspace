package pl.sda.zadnieZeSlacka;

public interface CzlonekRodziny {
    default void przedstawSie(){
        System.out.println("I'm just family member");
    }
    boolean jestDorosły();
}
