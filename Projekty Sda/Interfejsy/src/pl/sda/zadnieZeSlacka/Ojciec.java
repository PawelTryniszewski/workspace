package pl.sda.zadnieZeSlacka;

public class Ojciec implements CzlonekRodziny {
    private String imie;

    public String getImie() {
        return imie;
    }

    public Ojciec(String imie) {
        this.imie = imie;

    }

    public void zawolajOPiwo(){
        System.out.println("podaj piwo");
    }

    @Override
    public void przedstawSie() {
        System.out.println("Im your father "+getImie());

    }

    @Override
    public boolean jestDorosły() {
        return true;
    }
}
