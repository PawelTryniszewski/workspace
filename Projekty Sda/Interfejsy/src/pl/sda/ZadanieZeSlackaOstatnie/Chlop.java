package pl.sda.ZadanieZeSlackaOstatnie;

public class Chlop extends Citizen {
    public Chlop(String imie) {
        super(imie);
    }

    @Override
    public boolean canVote() {
        return false;
    }
}
