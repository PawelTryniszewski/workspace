package pl.sda.ZadanieZeSlackaOstatnie;

public class Zolnierze extends Citizen {
    public Zolnierze(String imie) {
        super(imie);
    }

    @Override
    public boolean canVote() {
        return true;
    }
}
