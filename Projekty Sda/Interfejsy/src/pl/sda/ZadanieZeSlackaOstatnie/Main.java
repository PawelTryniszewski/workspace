package pl.sda.ZadanieZeSlackaOstatnie;

import java.util.Arrays;

/**
 * Zadanie 4
 *  * Stwórz klasę abstrakcyjną Citizen oraz klasy dziedziczące:
 *  * Peasant(Chłop),
 *  * Townsman(Mieszczanin),
 *  * King(Król),
 *  * Soldier(Żołnierz)
 *  * Wszystkie klasy posiadają pole imie (przemyśl gdzie powinno się znajdować to pole). Citizen powinien
 *  * być klasą abstrakcyjną która posiada metodę abstrakcyjną 'canVote' która zwraca true dla townsman'a i
 *  * soldier'a, ale false dla chłopa i króla.
 *  *
 *  * Stwórz klasę Town która posiada tablicę Citizenów. Dodaj do niej kilku citizenów (różnych w mainie) i
 *  * stwórz metody howManyCanVote które zwracają ilość osób które mogą głosować. Stwórz w klasie Town metodę
 *  * "whoCanVote" która wypisuje imiona osób które mogą głosować.
 */

public class Main {
    public static void main(String[] args) {
        Mieszczanin pawel = new Mieszczanin("Pawel");
        Zolnierze adam = new Zolnierze("Adam");
        Krol dawid = new Krol("Dawid");
        Chlop bartek = new Chlop("Bartek");
        Citizen[] obywatele  = new Citizen[]{pawel,adam,dawid,bartek};
    }

    public int howManyCanVote(Citizen[] obywatele){
        for (Citizen obywatel:obywatele) {
            if (obywatel.canVote()==true)
                System.out.println(obywatel.imie);
            else
                System.out.println(obywatel.imie);

        }
        return 0;
    }
}
