package pl.sda.ZadanieZeSlackaOstatnie;

public abstract class Citizen {
    String imie;

    public Citizen(String imie) {
        this.imie = imie;
    }

    public abstract boolean canVote();
}
