package pl.sda.ZadanieZeSlackaOstatnie;

public class Krol extends Citizen {
    public Krol(String imie) {
        super(imie);
    }

    @Override
    public boolean canVote() {
        return false;
    }
}
