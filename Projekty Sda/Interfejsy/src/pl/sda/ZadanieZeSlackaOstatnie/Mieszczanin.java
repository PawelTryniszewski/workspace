package pl.sda.ZadanieZeSlackaOstatnie;

public class Mieszczanin extends Citizen {
    public Mieszczanin(String imie) {
        super(imie);
    }

    @Override
    public boolean canVote() {
        return false;
    }
}
