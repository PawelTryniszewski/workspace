package pl.sda.grzanie;

public class Farelka implements Grzej {
    private double temperatura;

    public Farelka(double temperatura) {
        this.temperatura = temperatura;
    }

    @Override
    public double pobierzTemp() {

        return temperatura;
    }

    @Override
    public void zwiekszTemp() {
        temperatura++;

    }
}
