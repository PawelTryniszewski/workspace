package pl.sda.grzanie;

public interface Grzej {
    double pobierzTemp();
    void zwiekszTemp();
    default void wyswietlTemp(){
        System.out.println("Aktualna temp w pomieszczeniu wynosi "+pobierzTemp());
    }
}
