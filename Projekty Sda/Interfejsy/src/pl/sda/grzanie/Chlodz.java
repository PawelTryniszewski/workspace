package pl.sda.grzanie;

public interface Chlodz {
    double pobierzTemp();
    void schlodz();
    default void wyswietlTemp(){
        System.out.println("Aktualna temp w pomieszczeniu wynosi "+pobierzTemp());
    }
}
