package pl.sda.grzanie;

public class Wiatrak implements Chlodz {
    private double temperatura;

    public Wiatrak(double temperatura) {
        this.temperatura = temperatura;
    }


    @Override
    public double pobierzTemp() {
        return temperatura;
    }

    @Override
    public void schlodz() {
         temperatura--;

    }

}
