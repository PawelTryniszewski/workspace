package pl.sda.grzanie;

public class Klima implements Grzej,Chlodz {
    private double temperatura;

    public Klima(double temperatura) {
        this.temperatura = temperatura;
    }

    @Override
    public void schlodz() {
        temperatura--;

    }

    @Override
    public double pobierzTemp() {
        return temperatura;
    }

    @Override
    public void zwiekszTemp() {
        temperatura++;

    }

    @Override
    public void wyswietlTemp() {
        System.out.println("Aktualna temp w pomieszczeniu wynosi "+pobierzTemp());

    }
}
