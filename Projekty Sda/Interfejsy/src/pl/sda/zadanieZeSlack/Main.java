package pl.sda.zadanieZeSlack;

import java.util.Scanner;

/**
 * Napisz interfejs ICalculable (obliczalne), a w interfejsie zadeklaruj metodę :
 * public double calculate()
 * Napisz klasę SumEquation która implementuje interfejs ICalculable i dodaj do klasy dwa pola (dwie liczby typu double).
 * Zaimplementuj metodę calculate() z interfejsu ICalculable tak, aby zwracała sumę obu pól.
 * Napisz klasę SubtractEquation która implementuje interfejs ICalculable i dodaj do klasy dwa pola (dwie liczby typu double).
 * Zaimplementuj metodę calculate() z interfejsu ICalculable tak, aby zwracała różnicę obu pól.
 * Napisz klasę DivEquation która implementuje interfejs ICalculable i dodaj do klasy dwa pola (dwie liczby typu double).
 * Zaimplementuj metodę calculate() z interfejsu ICalculable tak, aby zwracała wynik dzielenia pola a przez pole b.
 * Napisz klasę MulEquation która implementuje interfejs ICalculable i dodaj do klasy dwa pola (dwie liczby typu double).
 * Zaimplementuj metodę calculate() z interfejsu ICalculable tak, aby zwracała wynik mnożenia obu pól.
 * Stwórz w mainie aplikację, która przyjmuje komendy i dla komendy:
 * add - tworzy SumEquation i prosi o dwie zmienne (wartości pól), a następnie oblicza wartość sumy i wypisuje wynik
 * na ekran
 * sub - tworzy SubtractEquation i prosi o dwie zmienne (wartości pól), a następnie oblicza wartość różnicy i wypisuje
 * wynik na ekran
 * div - tworzy DivEquation i prosi o dwie zmienne (wartości pól), a następnie oblicza wynik dzielenia i wypisuje wynik na ekran
 * mul - tworzy MulEquation i prosi o dwie zmienne (wartości pól), a następnie oblicza wartość mnożenia i wypisuje wynik na ekran
 * Napisz klasę PowEquation która jako parametr przyjmuje wartość n oraz pow a następnie wylicza wartość liczby n do
 * potęgi pow. Dodaj obsługę tak, jak w przypadku wcześniejszych klas.
 */

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("wybierz\n add/sub/div/mul/pow");
        String komenda=scanner.next();


        switch (komenda){
            case "add":
                System.out.println("Podaj a");
                double a=scanner.nextDouble();
                System.out.println("Podaj b");
                double b=scanner.nextDouble();
                SumEquation suma = new SumEquation(a,b);
                System.out.println(suma.calculate());
                break;
            case "sub":
                System.out.println("Podaj a");
                double c=scanner.nextDouble();
                System.out.println("Podaj b");
                double d=scanner.nextDouble();
                SubtractEquation roznica = new SubtractEquation(c,d);
                System.out.println(roznica.calculate());
                break;
            case "mul":
                System.out.println("Podaj a");
                double e=scanner.nextDouble();
                System.out.println("Podaj b");
                double f=scanner.nextDouble();
                MulEquation mnozenie = new MulEquation(e,f);
                System.out.println(mnozenie.calculate());
                break;
            case "div":
                System.out.println("Podaj a");
                double g=scanner.nextDouble();
                System.out.println("Podaj b");
                double h=scanner.nextDouble();
                DivEquation dzielenie = new DivEquation(g,h);
                System.out.println(dzielenie.calculate());
                break;
            case "pow":
                System.out.println("Podaj a");
                double z =scanner.nextDouble();
                System.out.println("Podaj potege");
                int x=scanner.nextInt();
                PowerEquation pote = new PowerEquation(z,x);
                System.out.println(pote.calculate());
        }

    }
}
