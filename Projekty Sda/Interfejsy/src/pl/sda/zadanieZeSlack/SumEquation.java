package pl.sda.zadanieZeSlack;

public class SumEquation implements Calculabble {

    private double a;

    public SumEquation(double a, double b) {
        this.a = a;
        this.b = b;
    }

    private  double b;
    @Override
    public double calculate() {
        double suma=a+b;
        return suma;
    }
}
