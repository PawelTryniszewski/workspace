package pl.sda.zadanieZeSlack;

public class PowerEquation implements Calculabble{
    private double z;
    private int x;

    public PowerEquation(double z,int x) {
        this.z = z;
        this.x = x;
    }

    @Override
    public double calculate() {
        double pow = Math.pow(z,x);
        return pow;
    }
}
