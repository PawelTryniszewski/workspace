package pl.sda.zadanieZeSlack;

public class SubtractEquation implements Calculabble {
    private double a;
    private double b;

    public SubtractEquation(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double calculate() {
        double roznica = a-b;
        return roznica;
    }
}
