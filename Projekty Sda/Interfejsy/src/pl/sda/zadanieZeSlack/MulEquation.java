package pl.sda.zadanieZeSlack;

public class MulEquation implements Calculabble {
    private double a;
    private double b;

    public MulEquation(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double calculate() {
        double iloczyn=a*b;
        return iloczyn;
    }
}
