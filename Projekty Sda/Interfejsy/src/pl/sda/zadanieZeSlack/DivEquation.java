package pl.sda.zadanieZeSlack;

public class DivEquation implements Calculabble {

    private double a;
    private double b;

    public DivEquation(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double calculate() {
        double iloraz = a/b;
        return iloraz;
    }
}
