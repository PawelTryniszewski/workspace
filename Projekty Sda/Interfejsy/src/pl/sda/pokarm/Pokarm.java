package pl.sda.pokarm;

public class  Pokarm {
   private String Nazwa;
   private TypPokarmu typPokarmu;
   private double waga;

    public Pokarm(String nazwa, TypPokarmu typPokarmu, double waga) {
        Nazwa = nazwa;
        this.typPokarmu = typPokarmu;
        this.waga = waga;
    }

    public String getNazwa() {
        return Nazwa;
    }

    public TypPokarmu getTypPokarmu() {
        return typPokarmu;
    }

    public double getWaga() {
        return waga;
    }
}
