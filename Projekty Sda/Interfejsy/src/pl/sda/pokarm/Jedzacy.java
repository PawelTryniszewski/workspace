package pl.sda.pokarm;

public interface Jedzacy {
    void jedz(Pokarm pokarm);
    int ilePosilkow();
    int ileGramowZjedzonych();
}
