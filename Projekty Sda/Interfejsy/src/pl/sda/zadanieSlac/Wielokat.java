package pl.sda.zadanieSlac;

public abstract class Wielokat implements Figura {

    private double[] boki;

    public Wielokat(double[] boki) {
        this.boki = boki;
    }


    public double obliczObw() {
        double obwod = 0;
        for (double bok:boki) {
            obwod+=bok;
        }
        return obwod;
    }
}
