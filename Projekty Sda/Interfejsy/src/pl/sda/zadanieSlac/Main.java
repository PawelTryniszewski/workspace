package pl.sda.zadanieSlac;

/**
 * Zadanie 3
 * Napisz interfejs Figura który posiada metody:
 * public double calculateArea() - oblicza pole
 * public double calculateCircumference() - oblicza obwód
 * stwórz klasę Circle, Square i Rectangle które powinny posiadać odpowiednią ilość pól. Wszystkie powinny
 * implementować interfejs Figura (napisz obie metody we wszystkich klasach końcowych).
 * Stwórz w mainie pętlę parsującą komendy. Dopuszczalne komendy to:
 * square
 * rectangle
 * circle
 * Po których można wpisać wielkości boków (w zależności od kształtu). Następnie aplikacja pyta czy chcemy poznać
 * pole czy obwód wybranego kształtu i wypisuje wybraną wartość.
 *
 * Dodaj do klasy Figura metode paint():void która dla poszczególnych kształtów powinna je rysować na ekranie
 * za pomocą gwiazdek.
 *
 */

public class Main {
    public static void main(String[] args) {
        Okreg okrag = new Okreg(12);
        Kweadrat kwardat = new Kweadrat(4);
        Prostokat prostokat = new Prostokat(13,4);
        System.out.println(kwardat.obliczPole());
        kwardat.paint();
        System.out.println();
        prostokat.paint();
        System.out.println();
        okrag.paint();

    }
}
