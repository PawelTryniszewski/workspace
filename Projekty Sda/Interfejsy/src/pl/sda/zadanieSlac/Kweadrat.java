package pl.sda.zadanieSlac;

import java.util.Scanner;

public class Kweadrat extends Prostokat implements Figura {
    public Kweadrat(double a) {
        super(a, a);
    }

    @Override
    public String toString() {
        return String.format("Kwadrat o boku %.2f",a);
    }

    public void paint(){


        for (int i = 1; i <= a; i++) {
            for (int j = 0; j < a; j++) {
                System.out.print("* ");
            }
//            for (int k = 0; k < a; k++) {
//                System.out.print("*");
//            }
            System.out.print("\n");

        }
    }
}
