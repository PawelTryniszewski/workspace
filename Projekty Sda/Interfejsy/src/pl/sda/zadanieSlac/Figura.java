package pl.sda.zadanieSlac;

public interface Figura {
    double obliczPole();
    double obliczObw();
}
