package pl.sda.zadanieSlac;

public class Okreg implements Figura {

    protected final static double PI=3.14;
    protected double r;

    public Okreg(double r) {
        this.r = r;
    }

    @Override
    public double obliczObw() {
        return 2*PI*r;
    }

    @Override
    public double obliczPole() {
        return PI*r*r;
    }

    public void paint(){


    }
}
