package pl.sda.zadanieSlac;

public class Prostokat extends Czworokat implements Figura {

    protected double a,b;


    public Prostokat(double a, double b) {
        super(a, b, a, b);
        this.a=a;
        this.b=b;

    }

    @Override
    public String toString() {
        return String.format("Prostokat o bokach %.2f %.2f",a,b);
    }

    @Override
    public double obliczPole() {
        return a*b;
    }

    public void paint(){


        for (int i = 1; i <= b; i++) {
            for (int j = 0; j < a; j++) {
                System.out.print("* ");
            }
//            for (int k = 0; k < a; k++) {
//                System.out.print("*");
//            }
            System.out.print("\n");

        }
    }
}
