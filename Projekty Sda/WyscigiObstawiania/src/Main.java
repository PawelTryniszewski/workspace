import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Konta KontoGracza = new Konta(1, 100);
        Konta KontoBukmacherskie = new Konta(2, 1000000);

        Konie kon1 = new Konie("Mustang", "Czarny", 2014);

        Konie kon2 = new Konie("Pociagowy", "Bialy", 2016);

        Konie kon3 = new Konie("Arab", "Szary", 2007);

        Konie kon4 = new Konie("Andaluzyjski", "Kary", 2008);

        Konie kon5 = new Konie("Shire", "Brazowy", 2011);

            Konie[] Konietab = new Konie[]{kon1, kon2, kon3, kon4, kon5};

        do {


            System.out.println("Na ktorego konia stawiasz?");
            System.out.println("Mustang, Pociagowy, Arab, Andaluzyjski, Shire");
            //Mscanner.nextLine();
            String obstawianyKon = scanner.next();
            KontoGracza.wyswietlStanKonta();

            System.out.println("Ile chcesz postawic?");
            int obstawianaKwota = scanner.nextInt();


            if (KontoGracza.stanKonta / 2 >= obstawianaKwota) {
                KontoBukmacherskie.wplacSrodki(KontoGracza.pobierzSrodki(obstawianaKwota));
            } else if (KontoGracza.stanKonta / 2 <= obstawianaKwota) {
                System.out.println("Nie mozesz postawic wiecej niz polowa Twojego stanu konta.\n Wyścig sie rozpoczyna bez Ciebie");
            }



            Konie zwyciezca = null;
            do {

                for (Konie iter : Konietab) {
                    iter.wyscigNaDystanisie120();
                    if (iter.aktualnaPredkosc >= 120) {
                        zwyciezca = iter;
                        break;
                    }
                }
            } while (zwyciezca == null);


            if (obstawianyKon.equals(zwyciezca.nazwa)) {
                System.out.println("Gratulacje, Twoj kon zwyciężył!!");
                KontoGracza.wplacSrodki(KontoBukmacherskie.pobierzSrodki(obstawianaKwota * 2));
                KontoGracza.wyswietlStanKonta();
            } else {
                System.out.printf("Niestety źle obstawiłeś. zwyciezca to %s\n", zwyciezca.nazwa);
                KontoGracza.wyswietlStanKonta();
            }
         zwyciezca = null;
            kon1.aktualnaPredkosc=0;
            kon2.aktualnaPredkosc=0;
            kon3.aktualnaPredkosc=0;
            kon4.aktualnaPredkosc=0;
            kon5.aktualnaPredkosc=0;

        } while (KontoGracza.stanKonta >= 0);
    }

}

