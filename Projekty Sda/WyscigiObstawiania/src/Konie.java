import java.util.Random;

public class Konie {

    int predkosc = 0;
    int aktualnaPredkosc;
    String nazwa;
    protected String kolor;
    protected int rocznik;

    public Konie(String nazwa, String kolor, int rocznik) {
        this.nazwa = nazwa;
        this.kolor = kolor;
        this.rocznik = rocznik;
    }


    public void InfoOKoniu() {

        System.out.printf("%s Rocznik %d \nKolor %s  \n",nazwa,rocznik,kolor);


    }

    public void wyscigNaDystanisie120() {
        //while(aktualnaPredkosc<120){
        Random random = new Random();

        if (predkosc < 120) {
            this.aktualnaPredkosc += predkosc + random.nextInt(10);
            aktualnaPredkosc += predkosc;
            System.out.printf("Pokonal  %d km %s\n", aktualnaPredkosc, nazwa);
        }
        if (aktualnaPredkosc >= 120) {
            System.out.printf("%s Dobiegł pierwszy.\n", nazwa);
        }
//        }
    }

    @Override
    public String toString() {
        return String.format("%s kon rasy %s rocznik %d", kolor, nazwa, rocznik);
    }




}
