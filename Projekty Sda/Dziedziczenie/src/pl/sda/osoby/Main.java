package pl.sda.osoby;

public class Main {
    public static void main(String[] args) {
        Osoba Roman = new Osoba("roman","kowalski",25);
        Roman.przedstawSie();

        Student olek = new Student("Aleksander","Nowak",21,111);
        olek.przedstawSie();

        System.out.println(olek);

        Student bolek = new Student("boleslaw","Zielinski",34,111);

        System.out.println(olek.equals(bolek));
        System.out.println(olek.equals("boleslaw"));
    }
}
