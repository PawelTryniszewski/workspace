package pl.sda.auta;

public class Kabriolet extends Samochod {


    public Kabriolet(String nazwa, String kolor, int rocznik) {
        super(nazwa, kolor, rocznik);
    }

    public void schowajDach() {
        czySchowanyDach = true;
    }

    public void setCzySchowanyDach(boolean czySchowanyDach) {
        this.czySchowanyDach = czySchowanyDach;

    }

//    @Override
//    public void przyspieszODziesiec() {
//        while (aktualnaPredkosc < 180) {
//
//            if (predkosc < 180) {
//                this.aktualnaPredkosc += predkosc + 10;
//                aktualnaPredkosc += predkosc;
//                System.out.printf("Przyśpieszam do %d km/h\n", aktualnaPredkosc);
//            }
//            if (aktualnaPredkosc >= 180) {
//                System.out.println("Ograniczenie do 180");
//            }
//        }
//    }

//    @Override
//    public String toString() {
//        String s = super.toString() + " z rozsuwanym dachem";
//        return s;
//    }


}
