import java.util.ArrayList;

public class Pracownik {
    String imie;
    String nazwisko;
    double miesieczneWynagrodzenie;

    public Pracownik(String imie, String nazwisko, double miesieczneWynagrodzenie) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.miesieczneWynagrodzenie = miesieczneWynagrodzenie;
    }

    public double getMiesieczneWynagrodzenie() {
        return miesieczneWynagrodzenie;
    }

    public Pracownik() {
    }

    @Override
    public String toString() {
        return "Pracownik " +
                 imie +
                " " + nazwisko + ", Wynagrodzenie " + miesieczneWynagrodzenie+"\n";
    }
}
