public class SzybkiSamochod extends Samochod {

    SzybkiSamochod(String kolor, String marka, int rocznik, double aktualnaPredkosc) {
        super(kolor, marka, rocznik, aktualnaPredkosc);
    }

    @Override
    public void przyspiesz() {
        if (aktualnaPredkosc<200){
            aktualnaPredkosc+=predkosc+20;
            aktualnaPredkosc+=predkosc;
            System.out.println(aktualnaPredkosc);
        }
    }
}
