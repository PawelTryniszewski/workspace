import java.util.Objects;

public class Samochod {
    double predkosc=0;
    String kolor;
    String marka;

    public Samochod(String kolor, String marka, int rocznik,double aktualnaPredkosc) {
        this.kolor = kolor;
        this.marka = marka;
        this.rocznik = rocznik;
        this.aktualnaPredkosc=aktualnaPredkosc;
    }

    int rocznik;
    double aktualnaPredkosc;

    public void przyspiesz(){
        if (aktualnaPredkosc<140){
            aktualnaPredkosc+=predkosc+20;
            aktualnaPredkosc+=predkosc;
            System.out.println(aktualnaPredkosc);
        }
    }

    public String getKolor() {
        return kolor;
    }

    public String getMarka() {
        return marka;
    }

    public int getRocznik() {
        return rocznik;
    }

    @Override
    public String toString() {
        return  getMarka() +" kolor " + getKolor() +
                " rocznik " + getRocznik();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Samochod samochod = (Samochod) o;
        return rocznik == samochod.rocznik &&
                Objects.equals(kolor, samochod.kolor) &&
                Objects.equals(marka, samochod.marka);
    }

    @Override
    public int hashCode() {

        return Objects.hash(kolor, marka, rocznik);
    }
}
