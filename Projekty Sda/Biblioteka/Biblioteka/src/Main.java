import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Ksiazka nowa = new Ksiazka("Kod Leonarda","Dan Brown",2011);
        Ksiazka nowa1 = new Ksiazka("bbbb","ja",1964);
        Ksiazka nowa2 = new Ksiazka("cccc","ja",1964);
        Ksiazka nowa3 = new Ksiazka("dddd","ja",1964);
        Ksiazka nowa4 = new Ksiazka("eeee","ja",1964);

        ArrayList<Ksiazka> nowaLista=new ArrayList<>();
        nowaLista.add(nowa);
        nowaLista.add(nowa1);
        nowaLista.add(nowa2);
        nowaLista.add(nowa3);
        nowaLista.add(nowa4);
        Biblioteka biblioteka =new Biblioteka(nowaLista);
        biblioteka.wyswpitlDostepne();
        biblioteka.wypozyczKsiazke("bbbb");
        biblioteka.wyswpitlWypozyczone();

        System.out.println(nowa.toString());

    }
}
