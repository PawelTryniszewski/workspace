public class Ksiazka {
    String nazwa;
    String autor;
    int rokWydania;

    public Ksiazka(String nazwa, String autor, int rokWydania) {
        this.nazwa = nazwa;
        this.autor = autor;
        this.rokWydania = rokWydania;
    }

    @Override
    public String toString() {
        String info="'"+getNazwa()+"'"+", Autor "+getAutor()+", rok wydania "+getRokWydania()+".";
        return info;
    }

    public String getNazwa() {
        return nazwa;
    }

    public String getAutor() {
        return autor;
    }

    public int getRokWydania() {
        return rokWydania;
    }
}
