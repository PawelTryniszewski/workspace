public enum GatunekPiwa {
    LAGER("Lager",true),
    PILZNER("Pilzner",true),
    STOUT("Stout",false),
    PORTER("Porter",false),
    MIODOWE("Miodowe",false);

    private String name;
    private boolean czyJasne;

    GatunekPiwa(String name, boolean czyJasne) {
        this.name = name;
        this.czyJasne = czyJasne;
    }

    public String opisPiwa() {
        String rodzaj = new String();
        if (czyJasne)
            rodzaj="Jasne";
        else
            rodzaj="Ciemne";
        String opis = "Piwo "+name+" ("+rodzaj+")";
        return opis ;
    }


}
