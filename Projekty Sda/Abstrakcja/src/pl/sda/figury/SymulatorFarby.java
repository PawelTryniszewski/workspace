package pl.sda.figury;

public class SymulatorFarby {

    public static int ZapotrzebowanieNaFarbe(Figura[]ksztalty,double pojemnosc){

        double sumaPow=0;
        for (Figura figura:ksztalty) {
            sumaPow+=figura.obliczPole();
        }
        int iloscFarby= (int) (sumaPow/pojemnosc);
        if (sumaPow%pojemnosc!=0)
            iloscFarby+=1;
        return iloscFarby;
    }

    public static void main(String[] args) {
        Figura kwadrat = new Kweadrat(1);
        Figura kolo = new Okreg(1);
        Figura trapez = new Prostokat(1,1);

        Figura[] ksztalty = new Figura[]{kwadrat,kolo,trapez};


        System.out.println(ZapotrzebowanieNaFarbe(ksztalty,2));

    }
}
