package pl.sda.figury;

public class Main {
    public static void main(String[] args) {
        Okreg okrag = new Okreg(5);
        System.out.println(okrag.obliczObw());
        System.out.println(okrag.obliczPole());


        Kweadrat kwadrat = new Kweadrat(4);
        System.out.println(kwadrat.obliczObw());
        System.out.println(kwadrat.obliczPole());

        Figura[] figury = new Figura[]{okrag,kwadrat};
        double sumaPow=0;
        for (Figura figura:figury) {
            sumaPow+=figura.obliczPole();
        }
        System.out.println(sumaPow);

        Prostokat prostokąt = new Kweadrat(2.5);
        System.out.println(prostokąt);


    }
}
