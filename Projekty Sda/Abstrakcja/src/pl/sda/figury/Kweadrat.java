package pl.sda.figury;

public class Kweadrat extends Prostokat {
    public Kweadrat(double a) {
        super(a, a);
    }

    @Override
    public String toString() {
        return String.format("Kwadrat o boku %.2f",a);
    }
}
