package pl.sda.figury;

public abstract class Figura {
    public abstract double obliczObw();
    public abstract double obliczPole();
}
