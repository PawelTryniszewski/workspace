package pl.sda.biblioteka;

public class Autor extends Czlowiek {
    private String jezyk;

    public Autor(String imie, String nazwisko, String jezyk) {
        super(imie, nazwisko);
        this.jezyk = jezyk;
    }

    @Override
    public String toString() {
        return "Autor" + super.toString();
    }
}
