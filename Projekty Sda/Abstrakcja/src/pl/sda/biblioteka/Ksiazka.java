package pl.sda.biblioteka;

public class Ksiazka extends Egzemplarz {
    public String tytul;

    public Ksiazka(String tytul, Autor[] autorzy, int rokWydania, int iloscStron) {
        super(autorzy, rokWydania, iloscStron);
        this.tytul=tytul;
    }

    @Override
    public String pobierzTytul() {
        return tytul;
    }
}
