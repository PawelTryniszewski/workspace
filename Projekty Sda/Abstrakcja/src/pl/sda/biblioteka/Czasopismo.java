package pl.sda.biblioteka;

public class Czasopismo extends Egzemplarz {
    private String nazwaCzasopisma;
    private int numer;

    public Czasopismo( String nazwaCzasopisma, int numer, Autor[] autorzy, int rokWydania, int iloscStron) {
        super(autorzy, rokWydania, iloscStron);
        this.nazwaCzasopisma = nazwaCzasopisma;
        this.numer = numer;
    }



    @Override
    public String pobierzTytul() {
        return nazwaCzasopisma; //String.format("%s %d",nazwaCzasopisma,numer);
    }

}
