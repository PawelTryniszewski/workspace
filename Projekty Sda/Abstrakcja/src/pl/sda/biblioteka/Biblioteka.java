package pl.sda.biblioteka;

import java.util.concurrent.ExecutionException;

public class Biblioteka {
    private Egzemplarz[] zbior;
    Egzemplarz[] zbiorWypo = new Egzemplarz[10];


    public Biblioteka(Egzemplarz[] zbior) {
        this.zbior = zbior;

    }

    public Egzemplarz wypozyczKsiazke(String nazwa){
        for (int i = 0; i <zbior.length ; i++) {
            if (nazwa==zbior[i].pobierzTytul()){
                zbiorWypo[i]=zbior[i];
            }
        }
        return zbiorWypo[1];
    }

    public  boolean czyKsiazkaDostepna(String nazwa){
        boolean czyKsiazkaDostepna=false;
        for (Egzemplarz egzemplarz:zbior) {
            if (nazwa==egzemplarz.pobierzTytul())
                czyKsiazkaDostepna=true;
        }
        return czyKsiazkaDostepna;

    }

    public void wyswietlDostepne(){
        for (Egzemplarz egzemplarz:zbior) {
            System.out.println(egzemplarz);
        }
    }
    public void wyswietlWypozyczone(){
        for (Egzemplarz egzemplarz:zbiorWypo) {
            if (egzemplarz!=null)
            System.out.println(egzemplarz);
        }
    }


    public Egzemplarz[] szukajPoTytule(String tytul) {
        Egzemplarz[] znalezione = new Egzemplarz[10];
        int iloscZnalezionych = 0;
        for (Egzemplarz egzemplarz : zbior) {

            if (egzemplarz.pobierzTytul().contains(tytul)) {
                znalezione[iloscZnalezionych] = egzemplarz;
                iloscZnalezionych++;
            }

        }
        return przytnij(znalezione);
    }

    public Egzemplarz[] szukajPoAutorze(String autor){
        Egzemplarz[] znalezieniAutorzy = new Egzemplarz[10];
        int iloscZnalezionychAutorow = 0;
        for (Egzemplarz egzemplarz:zbior){
            if (egzemplarz.toString().contains(autor)){
                znalezieniAutorzy[iloscZnalezionychAutorow]=egzemplarz;
                iloscZnalezionychAutorow++;
            }
        }
        return przytnij(znalezieniAutorzy);
    }

    public Egzemplarz[] szukajPoTytuleAutorze(String tytul) {
        Egzemplarz[] znalezione = new Egzemplarz[10];
        int iloscZnalezionych = 0;
        for (Egzemplarz egzemplarz : zbior) {

            if (egzemplarz.toString().contains(tytul)) {
                znalezione[iloscZnalezionych] = egzemplarz;
                iloscZnalezionych++;
            }

        }
        return przytnij(znalezione);
    }





    private Egzemplarz[] przytnij(Egzemplarz[] pozycje) {

        int niePustePozycje = 0;
        for (Egzemplarz egzemplarz : pozycje) {
            if (egzemplarz != null) {
                niePustePozycje++;
            }

        }
        Egzemplarz[] przycieta = new Egzemplarz[niePustePozycje];
        for (int i = 0; i < przycieta.length; i++) {
            przycieta[i] = pozycje[i];
        }
        return przycieta;
    }
}
