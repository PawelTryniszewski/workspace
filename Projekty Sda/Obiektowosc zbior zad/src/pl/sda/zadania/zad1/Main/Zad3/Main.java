package pl.sda.zadania.zad1.Main.Zad3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Robot robot = new Robot(88, "Robo", false);
        String komenda;

        System.out.println("DOSTĘPNE KOMENDY:");
        System.out.println("Wlacz/Wylacz\nWymienbaterie/Laduj/Info/Ruch:\n                     " +
                "          ruch_reka_lewa/ruch_reka_prawa\n" +
                "                               krok_lewa/" +
                "krok_prawa\n                               Skok/Idz/Biegnij ");


        do {
            komenda = scanner.next().toLowerCase();
            if (komenda.equals("wymienbaterie")) {
                robot.wymienBaterie();
            }else if (komenda.equals("laduj")){
                robot.ladujZLadowarki();
            }
            else if (komenda.equals("wylacz")) {
                robot.wylaczRobota();
            } else if (komenda.equals("wlacz")) {
                robot.wlaczRobota();
            } else if (komenda.equals("ruch")) {
                if (robot.czyWlaczony) {
                    System.out.println("Jaki ruch wykonać?");
                    komenda = scanner.next().toUpperCase();
                    if (komenda.equals("BIEGNIJ")) {
                        robot.biegnij();
                    } else if (komenda.equals("IDZ")) {
                        robot.idzDoPrzodu();
                    } else {
                        try {
                            RuchRobota ruchRobota = RuchRobota.valueOf(komenda);
                            robot.poruszRobotem(ruchRobota);
                        } catch (IllegalArgumentException iae) {
                            System.out.println("Nie ma takiej komendy!");
                        }
                    }
                } else if (!robot.czyWlaczony) {
                    System.out.println(robot.getNazwaRobota() + " jest wyłączony");
                }
            } else if (komenda.equals("info")) {
                System.out.println(robot.toString());
            }
        } while (!komenda.equals("zamknij"));
    }
}



