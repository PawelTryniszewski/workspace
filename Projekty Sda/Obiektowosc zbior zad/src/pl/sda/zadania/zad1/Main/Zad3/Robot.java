package pl.sda.zadania.zad1.Main.Zad3;

import java.util.Scanner;

public class Robot {
    int poziomBaterii;
    String nazwaRobota;
    boolean czyWlaczony;

    public String getNazwaRobota() {
        return nazwaRobota;
    }

    public int getPoziomBaterii() {
        return poziomBaterii;
    }

    public Robot(int poziomBaterii, String nazwaRobota, boolean czyWlaczony) {
        this.poziomBaterii = poziomBaterii;

        this.nazwaRobota = nazwaRobota;
        this.czyWlaczony = czyWlaczony;
    }

    public void ladujZLadowarki() {
        if (poziomBaterii < 100) {
            long current = System.currentTimeMillis();
            while (poziomBaterii < 100) {
                if (System.currentTimeMillis() - current > 1000) {
                    int i = poziomBaterii + 1;
                    System.out.println("Laduje--> " + i + "%");
                    poziomBaterii++;
                    current = System.currentTimeMillis();
                }
            }

        } else if (poziomBaterii == 100) {
            System.out.println("Stan Baterii " + poziomBaterii + "%");
        }
    }

    public void idzDoPrzodu() {
        poruszRobotem(RuchRobota.KROK_LEWA);
        poruszRobotem(RuchRobota.KROK_PRAWA);
    }

    public void biegnij() {
        Scanner scanner = new Scanner(System.in);
        long current = System.currentTimeMillis();
        int licznik = 0;
        String nowaKomenda = "xxx";
        do {
            if (licznik < 5) {
                if (poziomBaterii >= RuchRobota.KROK_PRAWA.ileProcentBaterii) {

                    long current1 = System.currentTimeMillis();
                    int i1 = 1;
                    while (i1 >= 0) {
                        if (System.currentTimeMillis() - current > 300) {
                            i1--;
                            current = System.currentTimeMillis();
                        }
                    }
                    poruszRobotem(RuchRobota.KROK_PRAWA);
                    poruszRobotem(RuchRobota.KROK_LEWA);
                    long current2 = System.currentTimeMillis();
                    int i2 = 1;
                    while (i2 >= 0) {
                        if (System.currentTimeMillis() - current > 300) {
                            i2--;
                            current = System.currentTimeMillis();
                        }
                    }
                } else if (poziomBaterii < RuchRobota.KROK_PRAWA.ileProcentBaterii) {

                    System.out.println("Za malo mocy zeby kontynuowac.");
                    nowaKomenda = "t";
                }

            } else if (licznik >= 5) {
                System.out.println("stop? t/n");
                String s1 = scanner.next();
                switch (s1) {
                    case "t":
                        nowaKomenda = s1;
                        break;
                    case "n":
                        licznik = 0;
                }

            }
            licznik++;
        } while (nowaKomenda.equals("xxx"));

    }

    public void poruszRobotem(RuchRobota ruchRobota) {
        if (czyWlaczony) {
            if (poziomBaterii > 0) {
                if (poziomBaterii >= ruchRobota.ileProcentBaterii) {
                    System.out.println(nazwaRobota + " wykonuje " + ruchRobota);
                    poziomBaterii = poziomBaterii - ruchRobota.ileProcentBaterii;
                } else if (poziomBaterii < ruchRobota.ileProcentBaterii) {
                    System.out.println(nazwaRobota + " rozpoczyna " + ruchRobota + " ale zużywa resztę mocy i wyłącza się");
                    wylaczRobota();
                    poziomBaterii = 0;
                }

            }

        } else if (!czyWlaczony) {

            System.out.println("włącz robota");
        }


    }

    public void wymienBaterie() {
        if (czyWlaczony == false) {
            System.out.println("Bateria wymieniona");
            poziomBaterii = 100;
        } else
            System.out.println("Najpierw wyłącz robota");
    }

    public void wlaczRobota() {
        if (poziomBaterii > 0) {
            czyWlaczony = true;
        } else if (poziomBaterii <= 0) {
            System.out.println(nazwaRobota + " rozladowany. Wymien baterie, albo go naladuj");
            wylaczRobota();
        }
    }

    public void wylaczRobota() {
        czyWlaczony = false;
    }

    @Override
    public String toString() {
        return "Robot{" +
                "poziomBaterii=" + poziomBaterii +
                ", nazwaRobota='" + nazwaRobota + '\'' +
                ", czyWlaczony=" + czyWlaczony +
                '}';
    }
}
