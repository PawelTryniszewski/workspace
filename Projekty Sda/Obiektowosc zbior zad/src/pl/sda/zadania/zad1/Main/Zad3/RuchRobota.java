package pl.sda.zadania.zad1.Main.Zad3;

public enum RuchRobota {
    KROK_LEWA(2),
    KROK_PRAWA(2),
    RUCH_REKA_LEWA(1),
    RUCH_REKA_PRAWA(1),
    SKOK(4);

    int ileProcentBaterii;

    RuchRobota(int ileProcentBaterii) {
        this.ileProcentBaterii = ileProcentBaterii;
    }
}
