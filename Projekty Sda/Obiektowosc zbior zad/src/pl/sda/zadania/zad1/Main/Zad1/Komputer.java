package pl.sda.zadania.zad1.Main.Zad1;

public class Komputer {
    int moc;
    String producent;
    TypProcesora typ;

    public Komputer(int moc, String producent, TypProcesora typ) {
        this.moc = moc;
        this.producent = producent;
        this.typ = typ;
    }
}
