package pl.sda.zadania.zad1.Main.Zad4;

public class Rower {
    private int iloscPrzerzutek;
    private String nazwa;

    public Rower(int iloscPrzerzutek, String nazwa, TypRoweru typRoweru) {
        this.iloscPrzerzutek = iloscPrzerzutek;
        this.nazwa = nazwa;
        this.typRoweru = typRoweru;
    }

    TypRoweru typRoweru;

    @Override
    public String toString() {
        return
                "Nazwa= " + nazwa  +" IloscPrzerzutek= " + iloscPrzerzutek + ", Ilosc miejscu= " + typRoweru.iloscMiejsc;
    }
}
