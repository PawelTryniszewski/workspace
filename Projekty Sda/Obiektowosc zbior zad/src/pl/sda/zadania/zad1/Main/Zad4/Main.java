package pl.sda.zadania.zad1.Main.Zad4;

/**
 * Stwórz enum TypRoweru i daj mu dwie wartości: ROWER, TANDEM. Dodatkowo enum posiada pole o nazwie ilośćMiejsc.
 * Dla enuma ROWER pole ma wartość 1, dla TANDEM’u ma wartość 2. Stwórz metodę ‘pobierzIlośćMiejsc():int’.
 *
 * Stwórz klasę Rower która posiada pola:
 * ilośćPrzerzutek, typRoweru, nazwaRoweru
 *
 * Stwórz metodę ‘wypiszInformacjeORowerze():void” która wypisuje informacje oraz podaje/wypisuje ilość miejsc (w postaci liczby).
 */

public class Main {
    public static void main(String[] args) {
        Rower goral = new Rower(21,"Kroos",TypRoweru.ROWER);
        Rower tandem = new Rower(5,"Tandem",TypRoweru.TANDEM);
        System.out.println(goral.toString());
        System.out.println(tandem.toString());
    }
}
