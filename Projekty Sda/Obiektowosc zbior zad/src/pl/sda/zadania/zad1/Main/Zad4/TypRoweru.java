package pl.sda.zadania.zad1.Main.Zad4;

public enum TypRoweru {
    ROWER(1),
    TANDEM(2);
    int iloscMiejsc;

    TypRoweru(int iloscMiejsc) {
        this.iloscMiejsc = iloscMiejsc;
    }

    public int getIloscMiejsc() {
        return iloscMiejsc;
    }
}
