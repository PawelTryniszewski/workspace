package pl.sda.dataICzas.Domowe;

import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class CzasNaSwiecie {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String miasto;
        Map<String,String> zoneIds = new HashMap<>();
        zoneIds.put("Warszawa", "Europe/Warsaw");
        zoneIds.put("Paryż", "Europe/Paris");
        zoneIds.put("Whitehorse", "Etc/UTC");
        zoneIds.put("Moskwa", "Europe/Moscow");
        zoneIds.put("Canberra", "Australia/Sydney");
        do {
            System.out.println("Warszawa/Paryż/Whitehorse/Moscow/Canberra");
            miasto=scanner.next();
        switch (miasto){
            case"Warszawa":
                System.out.println(zoneIds.get("Warszawa")+" "+LocalTime.now());
                break;
            case"Paryż":
                System.out.println(zoneIds.get("Paryż")+" "+LocalTime.now().minusHours(1));
                break;
            case"Whitehorse":
                System.out.println(zoneIds.get("Whitehorse")+" "+LocalTime.now());
                break;
            case"Moscow":
                System.out.println(zoneIds.get("Moskwa")+" "+LocalTime.now().plusHours(1));
                break;
            case"Canberra":
                System.out.println(zoneIds.get("Canberra")+" "+LocalTime.now().plusHours(8));
                break;
        }

        }while (!miasto.equals("quit"));

    }
}
