package pl.sda.dataICzas.Zadania;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Scanner;

public class Zad2 {
    public static void main(String[] args) {

        //Zad2
        LocalDate data1 = LocalDate.now();
        System.out.println(data1.minusDays(10));
        System.out.println(data1.plusDays(10));

        //Zad3


        LocalDate data2 = LocalDate.now();
        data2 = data2.plusDays(12);
        Period period = Period.between(LocalDate.now(),data2);
        System.out.println(period);

        //Zad4
        Scanner scanner = new Scanner(System.in);
        String tekst = scanner.next();
        String tekst2 = scanner.next();


        LocalDate dateZTekstu = LocalDate.parse(tekst,DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        LocalDate dateZTekstu1 = LocalDate.parse(tekst2,DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        Period period1 = Period.between(dateZTekstu,dateZTekstu1);
        System.out.println(period1.getMonths());

        //Zad5
        System.out.println(period1.getYears());


    }
}
