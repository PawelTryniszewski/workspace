package pl.sda.dataICzas.Zadania;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Zad1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("wybierz date/time/datetime/quit");
        String polecenie;
        do {
            polecenie= scanner.next();


        switch (polecenie){
            case "date":
                LocalDate data1 = LocalDate.now();
                DateTimeFormatter dataFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                System.out.println(data1);
                break;
            case "time":
                LocalTime data2 = LocalTime.now();
                System.out.println(data2);
                break;
            case "datetime":
                LocalDateTime data13 = LocalDateTime.now();
                System.out.println(data13);
                break;
            case"quit":
                System.out.println("Koniec");
                break;

        }
        }while (!polecenie.equals("quit"));
    }
}
