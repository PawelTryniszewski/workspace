package pl.sda.dataICzas.zad7;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Random;
import java.util.Scanner;

public class Zad7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj date urodzenia");
        String data = scanner.next();
        LocalDate dateZTekstu = LocalDate.parse(data,DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        LocalDate data1 = LocalDate.now();
        data1 = dateZTekstu.plusYears(100);

        Period period1 = Period.between(data1,dateZTekstu);

        String czyMezczyzna;
        String czyPalisz;
        String czyStres;
        do {
            System.out.println("Jaka plec? m/k");
            czyMezczyzna = scanner.next();
        }while (!czyMezczyzna.equals("k")&&!czyMezczyzna.equals("m"));
        if (czyMezczyzna.equals("m")){
            data1 = data1.minusYears(10);
        }
        do {
        System.out.println("Czy palisz papierosy? t/n");

        czyPalisz = scanner.next();

        }while(!czyPalisz.equals("t")&&!czyPalisz.equals("n"));
        if (czyPalisz.equals("t")){
            data1= data1.minusYears(9).minusMonths(3);
        }

        do {
        System.out.println("Czy zyjesz w stresie? t/n");
        czyStres = scanner.next();

        }while(!czyStres.equals("t")&&!czyStres.equals("n"));

        if (czyStres.equals("t")) {
            Random random = new Random();
            int wylosowana = random.nextInt(100);
            if (wylosowana < 10) {
                data1 = dateZTekstu.plusYears(60);
            }
        }

        System.out.println("Umrzesz : " + data1);



    }
}
