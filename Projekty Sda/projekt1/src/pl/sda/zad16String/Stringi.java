package pl.sda.zad16String;

import java.text.AttributedCharacterIterator;

public class Stringi {
    public static void main(String[] args) {
        String tekst="Ania ma kota i psa";
        System.out.println(tekst.contains("Ania"));
        System.out.println((tekst.startsWith("Ania")));
        System.out.println(tekst.endsWith("Ania"));
        System.out.println(tekst.equals("Ania"));
        String tekst1 = tekst.toLowerCase();
        System.out.println(tekst1.contains("Ania"));
        System.out.println(tekst.indexOf("Ania"));
    }
}
