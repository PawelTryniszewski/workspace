package pl.sda.domowe.TypyDanychOperatoryPakietyInstrukcje.zad8;

public class SredniaOcen
{
    public static void main(String[] args)
    {
        double ocenaMatma = 5, ocenaChemia=4, ocenaPolski=3, ocenaAngielski = 6, ocenaWOS = 5, ocenaInformatyka=2;
        double sredniaWszystkich=(ocenaAngielski+ocenaChemia+ocenaInformatyka+ocenaMatma+ocenaPolski+ocenaWOS)/6;
        double sredniaScislych=(ocenaChemia+ocenaInformatyka+ocenaMatma)/3;
        double sredniaHuman=(ocenaChemia+ocenaInformatyka+ocenaMatma)/3;

        System.out.println("Srednia wszystkich ocen = "+(ocenaAngielski+ocenaChemia+ocenaInformatyka+ocenaMatma+ocenaPolski+ocenaWOS)/6);
        System.out.println("Srednia z przedmiotów ścisłych = "+(ocenaChemia+ocenaInformatyka+ocenaMatma)/3);
        System.out.println("Srednia z przedmiotów humanistycznych = "+(ocenaChemia+ocenaInformatyka+ocenaMatma)/3);

        if (ocenaAngielski == 1)
            System.out.println("Ocena z Angielskiego jest niedostateczna");
        else if (ocenaChemia==1)
            System.out.println("Ocena z Chemii jest niedostateczna");
        else if (ocenaInformatyka==1)
            System.out.println("Ocena z Informatyki jest niedostateczna");
        else if (ocenaMatma==1)
            System.out.println("Ocena z Matemetyki jest niedostateczna");
        else if (ocenaPolski==1)
            System.out.println("Ocena z j polskiego jest niedostateczna");
        else if (ocenaWOS==1)
            System.out.println("Ocena z WOSu jest niedostateczna");
        else if (sredniaWszystkich==1)
            System.out.println("Srednia ocen jest niedostateczna");
        else if (sredniaHuman==1)
            System.out.println("Srednia z Humana jest niedostateczna");
        else if (sredniaScislych==1)
            System.out.println("Srednia ze scislych jest niedostateczna");

    }
}
