package pl.sda.domowe.Stringi.KlasaStringWyrażeniaRegularne;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class CezarOdsz {

    public static String cezarwroc(String str) {     //metoda deszyfrujaca;
        char x[] = str.toCharArray();

        for (int i = 0; i != x.length; i++) {
            int n = x[i];
            n -= 5;
            x[i] = (char) n;
        }
        return new String(x);
    }
}

