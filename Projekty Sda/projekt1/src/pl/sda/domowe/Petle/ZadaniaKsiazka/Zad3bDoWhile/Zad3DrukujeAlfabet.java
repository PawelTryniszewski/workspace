package pl.sda.domowe.Petle.ZadaniaKsiazka.Zad3bDoWhile;

public class Zad3DrukujeAlfabet {
    public static void main(String[] args)
    {
        char znak;
        System.out.println("Program wyświetla duże litery alfabetu od A do Z i od Z do A.");
        znak = 'A';
        do
        {
            if (znak < 'Z')
                System.out.print(znak + ", ");
            else
                System.out.print(znak + ".");
            znak++;
        }
        while (znak <= 'Z');
        System.out.println();
        znak = 'Z';
        do
        {
            if (znak > 'A')
                System.out.print(znak + ", ");
            else
                System.out.print(znak + ".");
            znak--;
        }
        while (znak >= 'A');
    }
}
