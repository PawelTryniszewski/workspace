package pl.sda.domowe.Petle.ZadaniaKsiazka.Zad3cWhile;

/**
 * Napisz program, który przy wykorzystaniu instrukcji while
 * sumuje liczby całkowite od 1 do 100.
 */

public class Zad3iSumujeLiczby {
    public static void main(String[] args)
    {
        int i = 1, suma = 0;
        System.out.println("Program sumuje liczby całkowite od 1 do 100.");
        while (i <= 100)
        {
            suma = suma + i;
            i++;
        }
        System.out.println("Suma liczb całkowitych od 1 do 100 wynosi "
                + suma + ".");
    }
}
