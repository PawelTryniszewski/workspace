package pl.sda.domowe.Petle.ZadaniaKsiazka.Zad3aFor;

/**
 * Napisz program, który za pomocą instrukcji for dla danych
 * wartości x zmieniających się od 0 do 10 oblicza wartość
 * funkcji y = 3x.
 */

public class Zad3aObliczaFunkcje {

    public static void main(String[] args)
    {
        int x, y;
        System.out.println("Program oblicza wartość funkcji y = 3*x");
        System.out.println("dla x zmieniającego się od 0 do 10.");
        for (x = 0; x <= 10; x++)
        {
            y = 3*x;
            System.out.println("x = " + x + '\t' + "y = " + y);
        }
    }
}

