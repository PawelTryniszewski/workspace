package pl.sda.domowe.Petle.ZadaniaKsiazka.Zad3aFor;

public class Zad3mSumujeNieparzyste {

    public static void main(String[] args)
    {
        int i, suma = 0;
        System.out.print("Program sumuje liczby nieparzyste ");
        System.out.println("z przedziału od 1 do 100.");
        for (i = 1; i <= 100; i++)
        {
            if (!(i%2 == 0)) suma = suma+i;
        }
        System.out.print("Suma liczb nieparzystych z przedziału od 1 do 100 wynosi ");
                System.out.print(suma + ".");
    }
}
