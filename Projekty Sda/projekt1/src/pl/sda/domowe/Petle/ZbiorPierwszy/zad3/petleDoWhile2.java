package pl.sda.domowe.Petle.ZbiorPierwszy.zad3;

import java.util.Scanner;

/**
 * pobierz od użytkownika kolejną liczbę - dzielnik. Po pobraniu dzielnika
 * wypisz wszystkie liczby od początekZakresu do koniecZakresu które są
 * podzielne przez dzielnik.
 */

public class petleDoWhile2 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int poczatekZakresu, koniecZakresu,dzielnik;
        System.out.println("Podaj poczatek zakresu");
        poczatekZakresu=scanner.nextInt();

        do {
            System.out.println("Podaj koniec zakresu");
            koniecZakresu = scanner.nextInt();
            if (koniecZakresu>poczatekZakresu){
                System.out.println("\nDziekuje\n Teraz podaj dzielnik");
            }
            else
                System.out.println("");

        }while (koniecZakresu<poczatekZakresu);

        dzielnik= scanner.nextInt();

        for (int i = poczatekZakresu; i <koniecZakresu ; i++) {

            if (i%dzielnik==0)
                System.out.println(i);

        }
    }
}
