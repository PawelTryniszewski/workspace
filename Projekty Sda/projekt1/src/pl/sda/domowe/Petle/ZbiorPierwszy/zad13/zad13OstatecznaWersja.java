package pl.sda.domowe.Petle.ZbiorPierwszy.zad13;

import java.util.Scanner;

/**
 * Stwórz program który na wejściu przyjmuje liczbę N a następnie na wyjściu wypisuje
 * tabliczkę mnożenia do tej liczny (tj. do NxN).
 *
 * %4 oznacza długość 4 znaków, w przypadku gdy liczba ma mniej dodaje się spacje przed.
 * .0 oznacza 0 miejsc po przecinku
 * f oznacza float
 */

public class zad13OstatecznaWersja {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int i,j;
        int a;
        System.out.println("Podaj 'wielkość' tabliczki mnozenia");
        a=scanner.nextInt()+1;
        float tab[][] = new float[a][a];
        for (i=1;i<=tab.length-1;i++)
        {
            for (j=1;j<=tab[i].length-1;j++)
            {
                tab[i][j]=i*j;
            }
        }

        for (i=1;i<=tab.length-1;i++)
        {
            for (j=1;j<=tab[i].length-1;j++)
            {
                System.out.printf("%4.0f", tab[i][j]);
            }
            System.out.println(" ");
        }
    }
}
