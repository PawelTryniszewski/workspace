package pl.sda.domowe.Petle.ZbiorPierwszy.zad12;

import java.util.Scanner;

/**
 * Napisać program, który sprawdza, czy podana liczba całkowita N  jest większa od zera,
 * a następnie sprawdzająca czy liczba jest liczbą pierwszą.
 */

public class SprawdzaCzyJestLiczbaPierwsza {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double a;
        System.out.println("podaj liczbe wieksza od zera");
        do {
            a = scanner.nextDouble();
            if (a > 0)
                System.out.println("");
            else
                System.out.println("ta liczba jest mniejsza od 0");
        } while (a <= 0);

        for (int i = 1; i < a + 1; i++) {

            int podzielnik = 2;

            boolean liczbaPierwsza = true;

            if (i > 3) {

                while (podzielnik < i) {
                    if (i % podzielnik == 0) {

                        liczbaPierwsza = false;
                        break;

                    }
                    podzielnik++;
                }

            }
            if (liczbaPierwsza) {

                if (i == a)
                    System.out.println("Twoja liczba jest liczbą pierwszą");

            }

        }

    }


}

