package pl.sda.domowe.Petle.ZbiorPierwszy.zad15;

import java.util.Scanner;

public class PalindromTekst {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wpisz tekst");
        String tekst = scanner.nextLine();
        String odwroconyTekst = "";

        for (int i = tekst.length()-1; i>=0 ; i--) {
            odwroconyTekst=odwroconyTekst+tekst.charAt(i);
        }
        if (tekst.equals(odwroconyTekst))
            System.out.println("Podany tekst jest palindromem");
        else
            System.out.println("Podany tekst nie jest palindromem");
    }
}
