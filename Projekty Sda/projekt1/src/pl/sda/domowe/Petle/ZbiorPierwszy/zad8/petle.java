package pl.sda.domowe.Petle.ZbiorPierwszy.zad8;

import java.util.Scanner;

/**
 * *Napisać program, który pobiera od użytkownika ciąg liczb całkowitych. Pobieranie danych kończone jest
 * podaniem wartości 0 (nie wliczana do danych). W następnej kolejności program powinien wyświetlić sumę
 * największej oraz najmniejszej z podanych liczb oraz ich średnią arytmetyczną.
 * Spróbuj zadanie zrealizować z tablicą oraz bez tablicy
 * Pamiętaj! Czytaj liczby tak długo aż wczytana liczba nie jest 0!.
 */

public class petle {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int liczba;
        int[] tablicaLiczb = new int[20000];
        int licznikIndeksu = 0;
        do {
            liczba = scanner.nextInt();
//            for (int i = 0; i < tablicaLiczb.length; i++) {
//                tablicaLiczb[i] = liczba;
//            }
            tablicaLiczb[licznikIndeksu] = liczba;
            licznikIndeksu++;
        } while (liczba != 0);
        int min, max;
        min = tablicaLiczb[0];
        max = tablicaLiczb[0];
        for (int i = 0; i < licznikIndeksu - 1; i++) {
            if (tablicaLiczb[i] < min) {
                min = tablicaLiczb[i];
            }
            if (tablicaLiczb[i] > max) {
                max = tablicaLiczb[i];
            }
        }
        System.out.println(min + max);
        int suma = 0;
        for (int i = 0; i < licznikIndeksu - 1; i++) {
            suma += tablicaLiczb[i];
        }
        System.out.println((suma / (double)(licznikIndeksu-1)));




    }
}
