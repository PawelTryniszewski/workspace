package pl.sda.domowe.Petle.ZbiorPierwszy.zad16;

import java.util.Scanner;

/**
 * Program sumuje podane cyfry
 */

public class Sumowanko {
    public static void main(String[] args) {
        System.out.println("Podaj liczby (W jednel linii z odstępami)\nZeby zakończyć wpisz -1");
        int suma;
        do {
            Scanner scanner = new Scanner(System.in);
            int wynik = 0;
            suma = 0;
            String CzytaLinie = scanner.nextLine();
            scanner = new Scanner(CzytaLinie); //Pobiera info z CzytaLinie
            while (scanner.hasNextInt()) {
                wynik = scanner.nextInt();
                suma += wynik;
            }
            if (suma <= -1)
                System.out.println("Koniec programu");
            else if (suma >= 0)
                System.out.println("Suma = " + suma);

        } while (suma != -1);

    }

}
