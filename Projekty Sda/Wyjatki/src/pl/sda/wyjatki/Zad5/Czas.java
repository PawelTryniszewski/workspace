package pl.sda.wyjatki.Zad5;

import javafx.css.converter.DurationConverter;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Scanner;

/**
 * Stwórz aplikację i w niej załaduj od użytkownika datę i czas w przyszłości. Jeśli użytkowonik
 * wpisze datę/czas w złym formacie lub w przeszłości każ mu powtórzyć czynność (użyj pętli). Po
 * poprawnym wpisaniu daty oblicz ile czasu(w minutach) zostało do tamtej daty i wypisz na ekran.
 */

public class Czas {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj date z przyszlości");
        String data;
        LocalDate dateZTekstu;




        do {
            data = scanner.next();
            dateZTekstu = LocalDate.parse(data,DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        }while(!dateZTekstu.isAfter(LocalDate.now()));
//        System.out.println(dateZTekstu);
//        System.out.println(data);
//        System.out.println(LocalDate.now());

//
//        Period period = Period.between(LocalDate.now(),dateZTekstu);
//        System.out.println("Lat "+period.getYears()+" Miesięcy "+period.getMonths()+" Dni "+period.getDays());





        Duration duration = Duration.between(LocalDateTime.now(),dateZTekstu.atStartOfDay());
        System.out.println("Pozostało "+duration.toMinutes()+" minut");


    }
}
