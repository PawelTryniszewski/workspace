package pl.sda.wyjatki;

public class Main {
    private static void checkZero(int liczba) throws WlasnyBlad {
        if(liczba == 0){
            throw new WlasnyBlad(liczba);
        }
    }
    private static int metoda(){
        try {
            throw new Exception();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            System.out.println("Sprzątanie");
        }
        System.out.println("Finally");
        return 0;
    }

    private static void metodaRuntime(){
        throw new RuntimeException();
    }
}
