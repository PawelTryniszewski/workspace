package pl.sda.wyjatki.Zadania;

import java.util.Scanner;

public class Zad1 {
    public static void main(String[] args) {

        metoda(2,-1);


    }
    private static void metoda(int a, int b){
        try{
            if (b<0){
                throw new Exception("B<0");
            }
        }catch (Exception e){
            // co ma sie stac
            System.out.println("Wystapil blad,"+e.getMessage());
        }
    }

    private static void metoda2(int a, int b) throws Exception{
        if (b<0)
            throw new Exception("B<0");
    }

}
