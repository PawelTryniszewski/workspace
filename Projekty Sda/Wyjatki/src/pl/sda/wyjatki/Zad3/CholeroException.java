package pl.sda.wyjatki.Zad3;

public class CholeroException extends RuntimeException {
    public CholeroException(String message) {
        super(message);
    }
}
