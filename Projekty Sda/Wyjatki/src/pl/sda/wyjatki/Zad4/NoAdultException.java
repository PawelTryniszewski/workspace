package pl.sda.wyjatki.Zad4;

public class NoAdultException extends RuntimeException {
    public NoAdultException(String message) {
        super(message);
    }
}
