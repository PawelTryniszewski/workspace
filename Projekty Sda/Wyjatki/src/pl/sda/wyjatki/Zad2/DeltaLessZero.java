package pl.sda.wyjatki.Zad2;

public class DeltaLessZero extends RuntimeException {
    public DeltaLessZero(String message) {
        super(message);
    }
}
