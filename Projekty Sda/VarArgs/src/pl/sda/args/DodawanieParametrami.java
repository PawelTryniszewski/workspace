package pl.sda.args;

public class DodawanieParametrami {
    public static void main(String[] args) {
        int[] liczby= ZamienNaLiczby(args);

        int suma =0;
        for (int liczba:liczby){
            suma+=liczba;
        }
        System.out.println("Suma to "+suma);

    }
    public static int[] ZamienNaLiczby(String[] args) {
        int[] liczby = new int[args.length];

        for (int i=0; i<liczby.length; i++){
            liczby[i] = Integer.parseInt(args[i]);
        }

        return liczby;
    }

}
