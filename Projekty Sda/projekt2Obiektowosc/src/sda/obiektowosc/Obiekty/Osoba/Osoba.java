package sda.obiektowosc.Obiekty.Osoba;

public class Osoba {

    String imie;
    int rokUrodrenia;
    boolean płeć;

    public Osoba(String imie, int rokUrodrenia, boolean płeć) {
        this.imie = imie;
        this.rokUrodrenia = rokUrodrenia;
        this.płeć = płeć;
    }
    public Osoba(){}


    public static void main(String[] args) {
        Osoba ania = new Osoba();
        ania.imie= "Ania";
        ania.rokUrodrenia= 1990;
        ania.płeć =false;
        Osoba Andrzej = new Osoba();
        Andrzej.imie="Andrzej";
        Andrzej.rokUrodrenia=1964;
        Andrzej.płeć = true;
        Osoba Mariola = new Osoba();
        Mariola.imie ="Mariola";
        Mariola.rokUrodrenia= 1989;
        Mariola.płeć = false;

        Osoba Adam = new Osoba("Adam",1999,true);
        Osoba Daria = new Osoba("Daria",1992,false);
        Osoba Dawid = new Osoba("Dawid",1990,true);

        Osoba[] tabOsob = new Osoba[]{Adam,Daria,Dawid,Mariola,Andrzej,ania};

        System.out.println();

        String[] Panie = new String[20000];
        String[] Panowie = new String[20000];
        for (int i = 0; i <tabOsob.length ; i++) {
            if (tabOsob[i].płeć==true){

            }

        }



        Mariola.przedstawSie();
        ania.przedstawSie();
        Andrzej.przedstawSie();
    }

    private void przedstawSie() {
        int wiek = 2018-rokUrodrenia;
        System.out.printf("Cześć mam na imię %s i mam %d lat\n",imie,wiek);
        System.out.println();
    }

    private void wyswietlOsoby(Osoba[] tabOsob){
        for (int i = 0; i <tabOsob.length ; i++) {
            tabOsob[i].przedstawSie();
        }
    }

}
