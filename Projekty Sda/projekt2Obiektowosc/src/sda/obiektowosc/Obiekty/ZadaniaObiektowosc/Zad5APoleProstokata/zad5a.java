package sda.obiektowosc.Obiekty.ZadaniaObiektowosc.Zad5APoleProstokata;

import java.util.Scanner;

/**
 * program oblicza pole prostokata
 */

public class zad5a {
    double a,b, pole;

    public static void main(String[] args) {

        zad5a pole = new zad5a();

        pole.czytajDane();
        pole.przetworzDane();
        pole.wyswietlWynik();

    }


    public void czytajDane() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Program oblicza pole prostokąta");
        System.out.println("podaj a");
        a= scanner.nextDouble();
        System.out.println("podaj b");
        b=scanner.nextDouble();


    }
    public void przetworzDane(){
        pole=a*b;
    }

    public void wyswietlWynik(){
        System.out.print("Pole prostokąta o boku  a = ");
        System.out.printf(String.valueOf(a));
        System.out.print("i boku b = ");
        System.out.printf(String.valueOf(b));
        System.out.print("wynosi");
        System.out.printf(String.valueOf(pole));

    }
}



