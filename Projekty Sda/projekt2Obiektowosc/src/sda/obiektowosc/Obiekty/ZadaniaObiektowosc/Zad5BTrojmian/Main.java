package sda.obiektowosc.Obiekty.ZadaniaObiektowosc.Zad5BTrojmian;

import java.io.IOException;

public class Main {
    public static void main(String[] args)
            throws IOException
    {
        zad5b zad5b = new zad5b();
        zad5b.czytajDane();
        zad5b.przetworzDane();
        zad5b.wyswietlWynik();
    }
}
