package sda.obiektowosc.Obiekty.ZadaniaObiektowosc.Zad5BTrojmian;

import java.util.Scanner;

/**
 * program, który oblicza pierwiastki równania kwadratowego
 * ax2+bx+c = 0
 */

public class zad5b {

    double a, b, c, delta, x1, x2;
    char liczbaPierwiastkow;

    public static void main(String[] args) {


//        czytajDane();
//        przetworzDane();
//        wyswietlWynik();
    }


    public void czytajDane() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("program oblicza pierwiastki rownania kwadratowego");
        System.out.println("podaj a b oraz c");
        System.out.println("podaj a");
        a = scanner.nextDouble();

        if (a == 0) {
            System.out.println("Niedozwolona wartosc");
            System.exit(1);
        } else {
            System.out.println("Podaj b");
        }

        b = scanner.nextDouble();
        System.out.println("Podaj c");
        c = scanner.nextDouble();
    }

    public void przetworzDane() {
        delta = b * b - 4 * a * c;
        if (delta < 0) liczbaPierwiastkow = 0;
        if (delta == 0) liczbaPierwiastkow = 1;
        if (delta > 0) liczbaPierwiastkow = 2;

        switch (liczbaPierwiastkow) {
            case 1:
                x1 = -b / (2 * a);
                break;
            case 2: {
                x1 = (-b - Math.sqrt(delta)) / (2 * a);
                x2 = (-b + Math.sqrt(delta)) / (2 * a);
            }
            break;
        }


    }

    public void wyswietlWynik() {

        System.out.println("Dla wprowadzonych liczb");
        System.out.printf("a = " + "%2.2f,\n", a);
        System.out.printf("b = " + "%2.2f,\n", b);
        System.out.printf("c = " + "%2.2f,\n", c);

        switch (liczbaPierwiastkow) {
            case 0:
                System.out.print("brak pierwiastków rzeczywistych.");
                break;
            case 1:
                System.out.printf("trójmian ma jeden pierwiastek podwójny\n" +
                        "x1 = " + "%2.2f.\n", x1);
                break;
            case 2: {
                System.out.println("trójmian ma dwa pierwiastki");
                System.out.printf("x1 = " + "%2.2f,\n", x1);
                System.out.printf("x2 = " + "%2.2f.\n", x2);
            }
            break;
        }

    }
}
