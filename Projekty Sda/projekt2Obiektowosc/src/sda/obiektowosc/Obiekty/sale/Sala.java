package sda.obiektowosc.Obiekty.sale;

public class Sala {
    String nazwa;
    double iloscM2;
    int liczbaStanowisk;
    boolean rzutnik;
    boolean czyJestWolna=true;

    public Sala(String nazwe, double iloscM2, int liczbaStanowisk, boolean rzutnik) {
        this.nazwa = nazwe;
        this.iloscM2 = iloscM2;
        this.liczbaStanowisk = liczbaStanowisk;
        this.rzutnik = rzutnik;
    }
    public Sala(){
    }

    void wyswietlOpisSali(){
        String opis = String.format("Sala %s o pow. %.2f, " +
                "liczba stanowisk: %d", nazwa, iloscM2, liczbaStanowisk);
        if (rzutnik){
            opis += ", sala posiada rzutnik";
        }else {
            opis += ", sala nie posiada rzutnika";
        }
        System.out.println(opis);
    }

    //Alt+insert


}


