package sda.obiektowosc.Obiekty.graZgadnijHaslo;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        GraZgadnijHaslo gra = new GraZgadnijHaslo();

        while (!gra.czyKoniec()){
            System.out.println("Podaj liczbę");
            int liczba = scanner.nextInt();
            gra.czyToTenNumer(liczba);
        }

        if (!gra.czyZwyciestwo){
            System.out.println("Przegrałeś!");
        }


    }
}
