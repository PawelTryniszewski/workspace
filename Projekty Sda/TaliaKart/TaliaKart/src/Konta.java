public class Konta {

    private long numerKonta;
    public int stanKonta;

    public int getStanKonta() {
        return stanKonta;
    }

    public Konta(long numerKonta, int stanKonta) {
        this.numerKonta = numerKonta;
        this.stanKonta = stanKonta;
    }

    public long getNumerKonta() {
        return numerKonta;
    }
    public void setStanKonta(int stanKonta) {
        this.stanKonta = stanKonta;
    }

    public void wyswietlStanKonta(){
        System.out.println("Twoj stan konta"+ " wynosi "+stanKonta);
    }
    public void wplacSrodki(int kwota){
        stanKonta+=kwota;
    }
    public int pobierzSrodki(int kwota){
        if(kwota>stanKonta){
            return 0;

        }
        else{
            stanKonta-=kwota;
            return kwota;
        }
    }
}
