import java.util.EmptyStackException;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.Stack;

public class Main {


    public static void main(String[] args) {

        Stack stos = new Stack();
        Talia talia = new Talia(stos);
        Konta KontoGracza = new Konta(000011110000111, 100);
        Konta KontoKasyna = new Konta(000022220000222, 1000000);
        Scanner scanner = new Scanner(System.in);
        String komenda;
        int sumaPunktow = 0;
        int obstawianaKwota = 0;
        int wygraneGracza=0;
        int wygraneKasyna=0;
        Krupier krupier = new Krupier();

        do {
            komenda = scanner.next().toLowerCase();
            if (komenda.equals("potasuj")) {
                //talia.potasuj();
                Talia.getInstance().potasuj();
            } else if (komenda.equals("dobierz")) {
                try {
                    if (sumaPunktow == 0) {
                        sumaPunktow += talia.dobierzKarte();
                    }
                    try {
                        sumaPunktow += talia.dobierzKarte();
                        System.out.println("Twoja suma punktów: " + sumaPunktow);
                    } catch (EmptyStackException e) {
                        System.out.println("Koniec kart, potasuj nowa talię");
                    }
                    if (sumaPunktow > 21) {
                        System.out.println("Przegrales");
                        wygraneKasyna++;
                        if (obstawianaKwota > 0) {
                            KontoGracza.wyswietlStanKonta();
                            obstawianaKwota = 0;
                        }
                        sumaPunktow = 0;
                    }
                    if (sumaPunktow == 21) {
                        System.out.println("Wygrałeś!!");
                        wygraneGracza++;
                        if (obstawianaKwota > 0) {
                            KontoGracza.wplacSrodki(KontoKasyna.pobierzSrodki(obstawianaKwota * 2));
                            KontoGracza.wyswietlStanKonta();
                            obstawianaKwota = 0;
                        }
                        sumaPunktow = 0;
                    }
                } catch (EmptyStackException e) {
                    System.out.println("Potasuj karty");
                }
            } else if (komenda.equals("czekaj")) {
                try {

                    int wynikKrupiera = krupier.graj(talia);
                    if (wynikKrupiera == 21) {
                        System.out.println("Przegrałeś");
                        wygraneKasyna++;
                        if (obstawianaKwota > 0) {
                            KontoGracza.wyswietlStanKonta();
                            obstawianaKwota = 0;
                        }
                        wynikKrupiera = 0;
                        sumaPunktow = 0;
                    }
                    if (wynikKrupiera > 21) {
                        System.out.println("Wygrałeś");
                        wygraneGracza++;
                        if (obstawianaKwota > 0) {
                            KontoGracza.wplacSrodki(KontoKasyna.pobierzSrodki(obstawianaKwota * 2));
                            KontoGracza.wyswietlStanKonta();
                            obstawianaKwota = 0;
                        }
                        wynikKrupiera = 0;
                        sumaPunktow = 0;
                    }
                    if (wynikKrupiera < 21) {
                        if (wynikKrupiera > sumaPunktow) {
                            System.out.println("Przegrałeś.");
                            wygraneKasyna++;
                            if (obstawianaKwota > 0) {
                                KontoGracza.wyswietlStanKonta();
                                obstawianaKwota = 0;
                            }
                            wynikKrupiera = 0;
                            sumaPunktow = 0;
                        }
                        if (wynikKrupiera < sumaPunktow) {
                            System.out.println("Wygrałeś!");
                            wygraneGracza++;
                            if (obstawianaKwota > 0) {
                                KontoGracza.wplacSrodki(KontoKasyna.pobierzSrodki(obstawianaKwota * 2));
                                KontoGracza.wyswietlStanKonta();
                                obstawianaKwota = 0;
                            }
                            wynikKrupiera = 0;
                            sumaPunktow = 0;
                        }
                    }
                } catch (EmptyStackException e) {
                    System.out.println("Potasuj karty");
                }
            } else if (komenda.equals("postaw")) {
                if (sumaPunktow == 0 && obstawianaKwota == 0) {
                    System.out.println("Ile chcesz postawic?");
                    try {
                        obstawianaKwota = scanner.nextInt();
                        if (KontoGracza.stanKonta / 2 >= obstawianaKwota) {
                            KontoKasyna.wplacSrodki(KontoGracza.pobierzSrodki(obstawianaKwota));
                        } else if (KontoGracza.stanKonta / 2 <= obstawianaKwota) {
                            System.out.println("Nie mozesz postawic wiecej niz polowa Twojego stanu konta.\n ");
                            obstawianaKwota = 0;
                        }
                    }catch (InputMismatchException exception){
                        System.out.println("Zla wartość.");
                    }
                }
                if (obstawianaKwota>0){
                    System.out.println("Postawiłeś "+ obstawianaKwota);
                }
            }
        } while (!komenda.equals("koniec"));
        System.out.println();
        System.out.println("Wygrałeś "+wygraneGracza+" gier z "+(wygraneGracza+wygraneKasyna));

    }
}
