public enum Kolor {
    KIER("Kier"),
    KARO("Karo"),
    TREFL("Trefl"),
    PIK("Pik");


    String nazwa;

    Kolor(String nazwa) {
        this.nazwa = nazwa;
    }
    public String getNazwa(){
        return nazwa;
    }
}


