import java.util.Stack;

public class Talia {

    private final static Stack stos = new Stack();
    private final static Talia instance = new Talia(stos);
    public static Talia getInstance(){
        return instance;
    }

    private Stack<Karta> blackJack;

    public Talia(Stack<Karta> blackJack) {//  Zeby silngleton dzialal trzeba w konstruktorze dac PRIVATE!
        this.blackJack = blackJack;

    }

    @Override
    public String toString() {
        return "Talia{" + blackJack+"}" ;
    }

    private Karta AsPik = new Karta(Kolor.PIK, Figura.AS);
    private Karta AsKier = new Karta(Kolor.KIER, Figura.AS);
    private Karta AsTrefl = new Karta(Kolor.TREFL,Figura.AS);
    private Karta AsKaro = new Karta(Kolor.KARO,Figura.AS);
    private Karta KrolPik = new Karta(Kolor.PIK,Figura.KROL);
    private Karta KrolKier = new Karta(Kolor.KIER,Figura.KROL);
    private Karta KrolTrefl = new Karta(Kolor.TREFL,Figura.KROL);
    private Karta KrolKaro = new Karta(Kolor.KARO,Figura.KROL);
    private Karta DamaPik = new Karta(Kolor.PIK,Figura.DAMA);
    private Karta DamaKier = new Karta(Kolor.KIER,Figura.DAMA);
    private Karta DamaTrefl = new Karta(Kolor.TREFL,Figura.DAMA);
    private Karta DamaKaro = new Karta(Kolor.KARO,Figura.DAMA);
    private Karta WaletPik = new Karta(Kolor.PIK,Figura.WALET);
    private Karta WaletKier = new Karta(Kolor.KIER,Figura.WALET);
    private Karta WaletTrefl = new Karta(Kolor.TREFL,Figura.WALET);
    private Karta WaletKaro = new Karta(Kolor.KARO,Figura.WALET);
    private Karta DziesiatkaPik = new Karta(Kolor.PIK,Figura.DZIESIATKA);
    private Karta DziesiatkaKier = new Karta(Kolor.KIER,Figura.DZIESIATKA);
    private Karta DziesiatkaTrefl = new Karta(Kolor.TREFL,Figura.DZIESIATKA);
    private Karta DziesiatkaKaro = new Karta(Kolor.KARO,Figura.DZIESIATKA);
    private Karta DziewiatkaPik = new Karta(Kolor.PIK,Figura.DZIEWIATKA);
    private Karta DziewiatkaKier = new Karta(Kolor.KIER,Figura.DZIEWIATKA);
    private Karta DziewiatkaTrefl = new Karta(Kolor.TREFL,Figura.DZIEWIATKA);
    private Karta DziewiatkaKaro = new Karta(Kolor.KARO,Figura.DZIEWIATKA);
    private Karta OsemkaPik = new Karta(Kolor.PIK,Figura.OSEMKA);
    private Karta OsemkaKier = new Karta(Kolor.KIER,Figura.OSEMKA);
    private Karta OsemkaTrefl = new Karta(Kolor.TREFL,Figura.OSEMKA);
    private Karta OsemkaKaro = new Karta(Kolor.KARO,Figura.OSEMKA);
    private Karta SiudemkaPik = new Karta(Kolor.PIK,Figura.SIODEMKA);
    private Karta SiudemkaKier = new Karta(Kolor.KIER,Figura.SIODEMKA);
    private Karta SiudemkaTrefl = new Karta(Kolor.TREFL,Figura.SIODEMKA);
    private Karta SiudemkaKaro = new Karta(Kolor.KARO,Figura.SIODEMKA);
    private Karta SzostkaPik = new Karta(Kolor.PIK,Figura.SZOSTKA);
    private Karta SzostkaKier = new Karta(Kolor.KIER,Figura.SZOSTKA);
    private Karta SzostkaTrefl = new Karta(Kolor.TREFL,Figura.SZOSTKA);
    private Karta SzostkaKaro = new Karta(Kolor.KARO,Figura.SZOSTKA);
    private Karta PiatkaPik = new Karta(Kolor.PIK,Figura.PIĄTKA);
    private Karta PiatkaKier = new Karta(Kolor.KIER,Figura.PIĄTKA);
    private Karta PiatkaTrefl = new Karta(Kolor.TREFL,Figura.PIĄTKA);
    private Karta PiatkaKaro = new Karta(Kolor.KARO,Figura.PIĄTKA);
    private Karta CzworkaPik = new Karta(Kolor.PIK,Figura.CZWÓRKA);
    private Karta CzworkaKier = new Karta(Kolor.KIER,Figura.CZWÓRKA);
    private Karta CzworkaTrefl = new Karta(Kolor.TREFL,Figura.CZWÓRKA);
    private Karta CzworkaKaro = new Karta(Kolor.KARO,Figura.CZWÓRKA);
    private Karta TrojkaPik = new Karta(Kolor.PIK,Figura.TRÓJKA);
    private Karta TrojkaKier = new Karta(Kolor.KIER,Figura.TRÓJKA);
    private Karta TrojkaTrefl = new Karta(Kolor.TREFL,Figura.TRÓJKA);
    private Karta TrojkaKaro = new Karta(Kolor.KARO,Figura.TRÓJKA);
    private Karta DwojkaPik = new Karta(Kolor.PIK,Figura.DWÓJKA);
    private Karta DwojkaKier = new Karta(Kolor.KIER,Figura.DWÓJKA);
    private Karta DwojkaTrefl = new Karta(Kolor.TREFL,Figura.DWÓJKA);
    private Karta DwojkaKaro = new Karta(Kolor.KARO,Figura.DWÓJKA);

    private Karta[] kartyRosnaco = new Karta[]{DwojkaKaro,DwojkaTrefl,DwojkaKier,DwojkaPik,TrojkaKaro,TrojkaTrefl,TrojkaKier,
            TrojkaPik, CzworkaKaro,CzworkaTrefl,CzworkaKier,CzworkaPik,PiatkaKaro,PiatkaTrefl,PiatkaKier,PiatkaPik,
            SzostkaKaro,SzostkaTrefl,SzostkaKier,SzostkaPik, SiudemkaTrefl,SiudemkaKaro,SiudemkaKier,SiudemkaPik,
            OsemkaKaro,OsemkaTrefl,OsemkaKier,OsemkaPik,DziewiatkaKaro,DziewiatkaTrefl,DziewiatkaKier,DziewiatkaPik,
            DziesiatkaKaro,DziesiatkaTrefl,DziesiatkaKier,DziesiatkaPik,WaletKaro,WaletTrefl,WaletKier,WaletPik,
            DamaKaro,DamaTrefl,DamaKier,DamaPik,KrolKaro,KrolTrefl,KrolKier,KrolPik,AsKaro,AsTrefl,
            AsKier,AsPik};

    private Karta[] potasowaneKarty;

    public void potasuj(){
        potasowaneKarty = new Karta[52];
        int iloscKart=52;
        for (int i = 0; i < kartyRosnaco.length; i++) {
            int r = (int) (Math.random() * iloscKart);
            potasowaneKarty[i] = kartyRosnaco[r];
            kartyRosnaco[r] = kartyRosnaco[iloscKart - 1];
            iloscKart--;
        }
        blackJack.clear();
        for (int i = 0; i < potasowaneKarty.length; i++) {
            blackJack.push(potasowaneKarty[i]);

        }
    }
    public void pobierzTalie(){
        for (int i = 0; i < kartyRosnaco.length; i++) {
            blackJack.push(kartyRosnaco[i]);
        }
    }

    public boolean czySiepowtarzaja(){
        for (int i = 0; i < potasowaneKarty.length; i++) {
            for (int j = i+1; j < potasowaneKarty.length; j++) {
                if (potasowaneKarty[i].equals(potasowaneKarty[j])){
                    return true;
                }
            }
        }
        return false;
    }

    public int dobierzKarte(){
        System.out.println(blackJack.peek());
        int suma= blackJack.pop().figura.getWartosc();
        return suma;
    }

}
