package Ulamek;

public class Main {
    public static void main(String[] args){
        Ulamek pol= new Ulamek(1,2);
        pol.wyswietl();

        Ulamek cwierc = pol.pomnoz(pol);
        cwierc.wyswietl();

        Ulamek osiem = pol.pomnoz(pol).pomnoz(pol);
        osiem.wyswietl();

        Ulamek szesnastka = osiem.pomnoz(pol);
        szesnastka.wyswietl();

        Ulamek ulamek2 = new Ulamek(2,3);
        Ulamek ulamek4 = ulamek2.podziel(pol);
        ulamek4.wyswietl();
        Ulamek ulamek5 = ulamek4.dodaj(pol);
        ulamek5.wyswietl();
        Ulamek ulamek6= ulamek5.odejmij(ulamek4);
        ulamek6.wyswietl();


    }

}
