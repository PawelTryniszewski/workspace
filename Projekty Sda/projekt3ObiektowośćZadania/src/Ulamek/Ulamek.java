package Ulamek;

public class Ulamek {
    public static void main(String[] args) {

    }
    private int licznik;
    private int mianownik;

    public Ulamek(int licznik, int mianownik) {
        this.licznik = licznik;
        this.mianownik = mianownik;
        skroc();
    }

    private void skroc() {
        int nwd=1;
        for (int i=1; i<mianownik; i++){                   //skracanie ulamkow
            if (licznik%i==0 && mianownik%i==0){
                nwd = i;
            }
        }
        licznik=licznik/nwd;
        mianownik = mianownik/nwd;
    }

    public void wyswietl(){
        System.out.println(String.format("%d/%d",licznik,mianownik));
    }


    public Ulamek pomnoz(Ulamek ulamek) {
        int licznik = this.licznik * ulamek.licznik;
        int mianownik = this.mianownik * ulamek.mianownik;

        return new Ulamek(licznik, mianownik);
    }

    public Ulamek podziel(Ulamek ulamek) {
        int licznik = this.licznik * ulamek.mianownik;
        int mianownik = this.mianownik * ulamek.licznik;

        return new Ulamek(licznik, mianownik);
    }

    public Ulamek dodaj(Ulamek ulamek) {
        int licznik = this.licznik * ulamek.mianownik + ulamek.licznik * this.mianownik;
        int mianownik = this.mianownik * ulamek.mianownik;
        return new Ulamek(licznik, mianownik);
    }

    public Ulamek odejmij(Ulamek ulamek) {
        int licznik = this.licznik * ulamek.mianownik - ulamek.licznik * this.mianownik;
        int mianownik = this.mianownik * ulamek.mianownik;
        return new Ulamek(licznik, mianownik);
    }


}
