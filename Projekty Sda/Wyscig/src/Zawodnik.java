import java.util.Random;

public class Zawodnik {
    private String imie;
    int identyfikator;
    double predkoscMin;
    private double predkoscMax;
    double pokonanaOdleglość;

    public Zawodnik(String imie, int identyfikator, double predkoscMin, double predkoscMax) {
        this.imie = imie;
        this.identyfikator = identyfikator;
        this.predkoscMin = predkoscMin;
        this.predkoscMax = predkoscMax;
        this.pokonanaOdleglość = pokonanaOdleglość;
    }

    private void przedstawSie() {
        System.out.println(String.format("Mam na imię %s, moj nr to%d  biegam z predkoscia od %d do %d" +
                ". \n", imie, identyfikator,predkoscMin,predkoscMax));
    }

    public static void main(String[] args) {
        Zawodnik Pawel = new Zawodnik("Pawel", 1, 10.0, 20.0);
        Zawodnik Dawid = new Zawodnik("Dawid", 2, 5.0, 15);
        Zawodnik Mateusz = new Zawodnik("Mateusz", 3, 5.0, 20.0);

//        System.out.println(Pawel.pokonanaOdleglość);
//        Pawel.biegnij();
//        System.out.println(Pawel.pokonanaOdleglość);

        Zawodnik[] Zawodnicy = new Zawodnik[]{Pawel,Dawid,Mateusz};

        Zawodnik zwyciezca = null;

        do {
            for(Zawodnik zawodnik:Zawodnicy){
                zawodnik.biegnij();
                System.out.println(Pawel.pokonanaOdleglość+"     Pawel");
                System.out.println(Dawid.pokonanaOdleglość+"     Dawid");
                System.out.println(Mateusz.pokonanaOdleglość+"   Mateusz");
                if (zawodnik.pokonanaOdleglość>=100){
                    zwyciezca = zawodnik;
                    break;
                }
            }
        }while(zwyciezca==null);

        System.out.println("Zwyciezca "+zwyciezca.imie);
//        zwyciezca.przedstawSie();

    }

    private void biegnij() {
        Random random = new Random();
        this.pokonanaOdleglość += predkoscMin + random.nextDouble()*(predkoscMax-predkoscMin);

    }
}
